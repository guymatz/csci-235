//
// Created by Guy Matz on 3/16/21.
//

#ifndef PROJECT_03_DOUBLENODE_HPP
#define PROJECT_03_DOUBLENODE_HPP


template <typename ItemType>
class DoubleNode {
private:
    DoubleNode<ItemType>* next_ptr_;
    DoubleNode<ItemType>* prev_ptr_;
    ItemType stuff_;
public:
    DoubleNode();
    DoubleNode(const ItemType& s);
    void setItem(const ItemType& s);
    bool setNext(DoubleNode<ItemType>* ptr);
    bool setPrev(DoubleNode<ItemType>* ptr);
    DoubleNode<ItemType>* getNext();
    DoubleNode<ItemType>* getPrev();
    ItemType getItem() const;
};

#include "DoubleNode.cpp"

#endif //PROJECT_03_DOUBLENODE_HPP