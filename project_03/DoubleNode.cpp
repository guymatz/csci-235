//
// Created by Guy Matz on 3/16/21.
//

//#include "DoubleNode.hpp"

template <typename ItemType>
DoubleNode<ItemType>::DoubleNode() {
    next_ptr_ = nullptr;
    prev_ptr_ = nullptr;
}

template <typename ItemType>
DoubleNode<ItemType>::DoubleNode(const ItemType& stuff) {
    stuff_ = stuff;
    next_ptr_ = nullptr;
    prev_ptr_ = nullptr;
}

template <typename ItemType>
void DoubleNode<ItemType>::setItem(const ItemType& stuff) {
    //std::cout << "I see " << stuff << std::endl;
    stuff_ = stuff;
}

template <typename ItemType>
bool DoubleNode<ItemType>::setNext(DoubleNode<ItemType>* ptr) {
    next_ptr_ = ptr;
    return true;
}

template <typename ItemType>
ItemType DoubleNode<ItemType>::getItem() const {
    return stuff_;
}

template <typename ItemType>
bool DoubleNode<ItemType>::setPrev(DoubleNode<ItemType>* ptr) {
    prev_ptr_ = ptr;
    return true;
}

template <typename ItemType>
DoubleNode<ItemType>* DoubleNode<ItemType>::getPrev() {
    //std::cout << "In getPrev " << prev_ptr_ << std::endl;
    return prev_ptr_;
}

template <typename ItemType>
DoubleNode<ItemType>* DoubleNode<ItemType>::getNext() {
    return next_ptr_;
}