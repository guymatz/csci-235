//
// Created by Guy Matz on 3/16/21.
//

#ifndef PROJECT_03_DOUBLYLINKEDLIST_HPP
#define PROJECT_03_DOUBLYLINKEDLIST_HPP

#include "DoubleNode.hpp"
#include <iostream>

template <typename ItemType>
class DoublyLinkedList {
private:
    std::size_t count_;
    DoubleNode<ItemType>* head_ptr_;
    DoubleNode<ItemType>* tail_ptr_;
    DoubleNode<ItemType>* getPointerTo(const int& position) const;

public:
    DoublyLinkedList();
    DoublyLinkedList(const DoublyLinkedList<ItemType>& list );
    ~DoublyLinkedList();

    // returns the number of the nodes in the calling list
    int getSize() const;
    // returns whether the calling list is empty
    bool isEmpty() const;
    //inserts item at position in caller list
    bool insert(const ItemType &item, const int& position);
    //removes the node at position
    bool remove(const int& position);
    // clears the list
    void clear();
    // returns a copy of the hearPtr
    DoubleNode<ItemType>* getHeadPtr() const;
    // returns a copy of the tail_ptr
    DoubleNode<ItemType>* getTailPtr() const;
    // returns a pointer to the node located at pos
    DoubleNode<ItemType>* getAtPos(const int& pos) const;
    // prints the contents of the calling list in order
    void display() const;
    // prints the contents of the calling list in reverse order
    void displayBackwards() const;
    // returns the interleaved list of the calling and parameter lists
    DoublyLinkedList<ItemType> interleave(const DoublyLinkedList<ItemType> &a_list);
};


#include "DoublyLinkedList.cpp"
#endif //PROJECT_03_DOUBLYLINKEDLIST_HPP
