//
// Created by Guy Matz on 3/16/21.
//
//#include "DoublyLinkedList.hpp"

template <typename ItemType>
DoublyLinkedList<ItemType>::DoublyLinkedList() : head_ptr_(nullptr), tail_ptr_(nullptr), count_(0) {
}

template <typename ItemType>
DoublyLinkedList<ItemType>::DoublyLinkedList(const DoublyLinkedList<ItemType>& dll) {

    count_ = 0;
    head_ptr_ = nullptr;
    tail_ptr_ = nullptr;
    DoubleNode<ItemType>* p = dll.getHeadPtr();

    // loop through the passed in dll and insert its elements into this one
    for (int i = 1; i <= dll.getSize(); i++) {
        //std::cout << "Inserting " << p->getItem() << " into spot " << i << std::endl;
        insert(p->getItem(), i);
        p = p->getNext();
    }
}

template <typename ItemType>
DoublyLinkedList<ItemType>::~DoublyLinkedList() {
    clear();
}

template <typename ItemType>
int DoublyLinkedList<ItemType>::getSize() const {
    return count_;
}

// returns whether the calling list is empty
template <typename ItemType>
bool DoublyLinkedList<ItemType>::isEmpty() const {
    if (count_ > 0) {
        return false;
    }
    else {
        return true;
    }
}

//inserts item at position in caller list
template <typename ItemType>
bool DoublyLinkedList<ItemType>::insert(const ItemType& item, const int& position) {
    DoubleNode<ItemType>* new_node_ptr = new DoubleNode<ItemType>(item);
    //std::cout << "In insert  - " << new_node_ptr->getItem() << " " << new_node_ptr << std::endl;
    DoubleNode<ItemType>* pos_ptr = getAtPos(position);

    if (head_ptr_ == nullptr) {
        // First Insert
        //std::cout << "First at Front" << std::endl;
        //std::cout << "New Node next = nullptr" << std::endl;
        new_node_ptr->setNext(nullptr);
        //std::cout << "New Node prev = nullptr" << std::endl;
        new_node_ptr->setPrev(nullptr);
        head_ptr_ = new_node_ptr;
        tail_ptr_ = new_node_ptr;
    }
    else if (pos_ptr == nullptr) {
        // Inserting at end
        //std::cout << "Inserting at end" << std::endl;
        //std::cout << "New Node next = nullptr" << std::endl;
        new_node_ptr->setNext(nullptr);
        //std::cout << "New Node prev = tail_ptr " << tail_ptr_ << std::endl;
        new_node_ptr->setPrev(tail_ptr_);
        //std::cout << "prev next " << new_node_ptr << std::endl;
        tail_ptr_->setNext(new_node_ptr);
        //std::cout << "Set tail to be " << new_node_ptr << std::endl;
        tail_ptr_ = new_node_ptr;
    }
    else {
        // Insert in middle
        //std::cout << "Inserting in pos " << position << std::endl;
        //std::cout << "Set New Node next = " << pos_ptr << std::endl;
        new_node_ptr->setNext(pos_ptr);
        //std::cout << "Set New Node prev = " << pos_ptr->getPrev() << std::endl;
        new_node_ptr->setPrev(pos_ptr->getPrev());
        if (pos_ptr->getPrev() != nullptr) {
            //std::cout << "Set " << pos_ptr << " next = " << new_node_ptr << std::endl;
            pos_ptr->getPrev()->setNext(new_node_ptr);
        }
        else {
            head_ptr_ = new_node_ptr;
        }
        //std::cout << "Set " << pos_ptr << " prev = " << new_node_ptr << std::endl;
        pos_ptr->setPrev(new_node_ptr);
    }
    count_++;
    //std::cout << "Count is now " << count_ << std::endl;
    return true;
}

// returns a pointer to the node located at pos
template <typename ItemType>
DoubleNode<ItemType>* DoublyLinkedList<ItemType>::getAtPos(const int& position) const {
    //std::cout << "Looking for pos " << position << std::endl;
    DoubleNode<ItemType>* node = head_ptr_;
    if (position > count_) {
        //std::cout << "Returning null for pos " << position << ", " << count_ << std::endl;
        return nullptr;
    }
    int p=0;
    for (int i=1; i < position; i++) {
        //std::cout << "Looking in pos " << i << ", node is " << node << std::endl;
        node  = node->getNext();
        p = i+1;
    }
    //std::cout << "Found " << node << " " << node->getItem() << " in pos " << p << std::endl;
    return node;
}

//removes the node at position
template <typename ItemType>
bool DoublyLinkedList<ItemType>::remove(const int& position) {
    // get next and prev nodes, relink them and remove node
    DoubleNode<ItemType>* node = getAtPos(position);
    //std::cout << "In remove: count is " << count_ << ", node is " << node->getItem() << " " << node << std::endl;
    DoubleNode<ItemType>* prev_node = node->getPrev();
    DoubleNode<ItemType>* next_node = node->getNext();

    if (prev_node == nullptr) {
        //std::cout << "In remove: next_node is " << next_node->getItem() << " " << next_node << std::endl;
        next_node->setPrev(nullptr);
        head_ptr_ = next_node;
    }
    else if (next_node == nullptr) {
        //std::cout << "In remove: prev_node is " << prev_node->getItem() << " " << prev_node << std::endl;
        prev_node->setNext(nullptr);
        tail_ptr_ = prev_node;
    }
    else {
        prev_node->setNext(next_node);
        next_node->setPrev(prev_node);
    }
    delete node;
    count_--;
    node = nullptr;
    return true;
}

// clears the list
template <typename ItemType>
void DoublyLinkedList<ItemType>::clear() {
    if (count_ == 0) {
        return;
    }
    DoubleNode<ItemType>* curr_ptr = head_ptr_;
    do {
        DoubleNode<ItemType>* next_ptr = curr_ptr->getNext();
        //std::cout << "In clear, deleting " << curr_ptr << std::endl;
        curr_ptr->setNext(nullptr);
        curr_ptr->setPrev(nullptr);
        delete curr_ptr;
        count_--;
        curr_ptr = next_ptr;
    } while (curr_ptr != nullptr);
    head_ptr_ = nullptr;
    tail_ptr_ = nullptr;
    //std::cout << "In clear, Count is now " << count_ << std::endl;
}

// returns a copy of the hearPtr
template <typename ItemType>
DoubleNode<ItemType>* DoublyLinkedList<ItemType>::getHeadPtr() const {
    //std::cout << "In getHeadPtr" << std::endl;
    return head_ptr_;
}

// prints the contents of the calling list in order
template <typename ItemType>
void DoublyLinkedList<ItemType>::display() const {
    DoubleNode<ItemType>* curr_ptr = head_ptr_;
    //std::cout << "In Display" << std::endl;
    for (int i=1; i <= count_; i++) {
        //std::cout << i << " " << curr_ptr->getItem() << " " << curr_ptr << std::endl;
        //std::cout << i << ":" << curr_ptr << ":" << curr_ptr->getItem() << " ";
        std::cout << curr_ptr->getItem();
        // no space after last
        if (i < count_) {
            std::cout << " ";
        }
        curr_ptr = curr_ptr->getNext();
    }
    std::cout << std::endl;
    //std::cout << "Leaving Display" << std::endl;
}

// prints the contents of the calling list in reverse order
template <typename ItemType>
void DoublyLinkedList<ItemType>::displayBackwards() const {
    //std::cout << "In DisplayBackwards" << std::endl;
    if (count_ > 0) {
        DoubleNode<ItemType>* curr_node = tail_ptr_;
        //std::cout << &curr_node << " " << curr_node->getPrev() << std::endl;
        for (int i = count_; i > 0 ; i--) {
            //std::cout << i << ":" << curr_node->getItem() << " ";
            std::cout << curr_node->getItem();
            // no space after last
            if (i > 1) {
                std::cout << " ";
            }
            curr_node = curr_node->getPrev();
        }
    }
    std::cout << std::endl;
}

// returns the interleaved list of the calling and parameter lists
/*
 * Interleave Example: Define the calling list as a set of ordered nodes,
 * $L1 = {4, 2, 8 ,5, 8}$, and define the list that is passed as a parameter as a set of
 * ordered nodes, $L2 = {5, 1, 8, 4, 5, 9}$. L1.interleave(L2) yields the set
 * ${4, 5, 2, 1, 8, 8, 5, 4, 8, 5, 9}$. In other words, to create the interleaved list,
 * first add a node from L1, then one from L2, and then repeat as many times as
 * necessary. If there are any nodes left over in L1 or L2 exclusively, append them to
 * the end of the list.
 */
template <typename ItemType>
DoublyLinkedList<ItemType> DoublyLinkedList<ItemType>::interleave(const DoublyLinkedList<ItemType> &a_list) {
    DoublyLinkedList<ItemType> dll;
    DoubleNode<ItemType>* p1 = getHeadPtr();
    DoubleNode<ItemType>* p2 = a_list.getHeadPtr();
    bool p1_done = false;
    bool p2_done = false;
    int i = 1;
    do {
        if (p1 == nullptr) {
            //std::cout << "p1 is null" << std::endl;
            p1_done = true;
        }
        else {
            //std::cout << "Adding from p1 " << p1->getItem() << std::endl;
            dll.insert(p1->getItem(), i);
            p1 = p1->getNext();
            i++;
        }
        if (p2 == nullptr) {
            //std::cout << "p2 is null" << std::endl;
            p2_done = true;
        }
        else {
            //std::cout << "Adding from p2 " << p2->getItem() << std::endl;
            dll.insert(p2->getItem(), i);
            p2 = p2->getNext();
            i++;
        }
    } while (! (p1_done and p2_done) );
    return dll;
}