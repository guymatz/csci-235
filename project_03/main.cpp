#include <iostream>
//#include "DoubleNode.hpp"
#include "DoublyLinkedList.hpp"

int main() {
    std::cout << "Hello, World!" << std::endl;

    /*
    DoubleNode n1 = DoubleNode<std::string>("poopy");
    DoubleNode n2 = DoubleNode<std::string>("doopy");
    std::cout << "n says " << n1.getItem() << std::endl;
    n1.setItem("stoopy");
    std::cout << "n says " << n1.getItem() << std::endl;
    DoubleNode<std::string>* n1_ptr = &n1;
    DoubleNode<std::string>* n2_ptr = &n2;
    n1.setPrev(nullptr);
    n1.setNext(n2_ptr);
    n2.setPrev(n1_ptr);
    n2.setNext(nullptr);
    */
    DoublyLinkedList<std::string> dll;
    dll.display();
    dll.insert("1", 1);
    dll.display();
    dll.insert("2", 2);
    dll.display();
    dll.insert("3", 1);
    dll.display();
    dll.insert("4", 4);
    dll.display();
    dll.insert("5", 3);
    dll.display();
    dll.clear();

    dll.insert("1", 1);
    dll.insert("2", 2);
    dll.insert("3", 3);
    dll.insert("4", 4);
    dll.insert("5", 5);
    dll.insert("6", 6);
    /*
    bool huh = dll.remove(2);
    dll.display();
    dll.displayBackwards();
    dll.clear();
    dll.display();
    */

    DoublyLinkedList<std::string> sdll;
    sdll.display();
    sdll.insert("uno", 1);
    sdll.display();
    sdll.insert("dos", 2);
    sdll.display();
    sdll.insert("tres", 3);
    sdll.insert("quattro", 4);
    sdll.display();
    sdll.displayBackwards();
    sdll.remove(4);
    sdll.display();
    sdll.remove(1);
    sdll.display();
    sdll.remove(2);
    sdll.display();
    sdll.insert("uno", 0);
    sdll.insert("dos", 1);
    sdll.insert("tres", 1);
    sdll.insert("quattro", 1);
    sdll.insert("poopy", 3);
    sdll.display();
    sdll.clear();
    sdll.insert("uno", 1);
    sdll.insert("dos", 2);
    sdll.insert("tres", 3);
    sdll.insert("quatro", 4);

    dll.display();
    sdll.display();
    DoublyLinkedList<std::string> idll = dll.interleave(sdll);
    std::cout << "idll" << std::endl;
    idll.display();

    DoublyLinkedList<std::string> cc_dll = dll;
    std::cout << "well?" << std::endl;
    dll.display();
    cc_dll.display();


    return 0;
}
