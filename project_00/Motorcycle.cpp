#include <iostream>
#include <cstdlib>
#include <ctime>
#include "Motorcycle.hpp"
using namespace std;

Motorcycle::Motorcycle() {
    srand (time(NULL));
    // do I need to hard-code the size of bike_type here?  :-(
    int bt = rand() % 4;
    //cout << "bt = " << bt << endl;
    brand_ = bike_details::bike_brand(bt);
    curr_speed_ = 0.0;
    curr_direction_ = 0.0;
    distance_traveled_ = 0.0;
    curr_acceleration_  = bike_details::NONE;
}

Motorcycle::Motorcycle(int kind_of_bike) {
    brand_ = bike_details::bike_brand(kind_of_bike);
    curr_speed_ = 0.0;
    curr_direction_ = 0.0;
    distance_traveled_ = 0.0;
    curr_acceleration_ = bike_details::NONE;
}

/*
float Motorcycle::getCurrDir() {
    return curr_direction_;
}
*/

/**
    return the string that corresponds to the curr_direction_ that the caller faces
        relative to a cartesian plane; assume that the caller is at position (0,0),
        the degree measure of (1, 0) is 0 degrees, and the degree measure of (-1, 0) is 180 degrees

        "North" == 90
        0 < "Northeast" < 90
        "East" == 0
        "Southeast" > 270
        "South" == 270
        180 < "Southwest" < 270
        "West" == 180
        90 < "Northwest" < 180
*/
std::string Motorcycle::getDirection() {
    if (curr_direction_ == 0.0) {
        return "East";
    }
    else if (curr_direction_ < 90) {
        return "Northeast";
    }
    else if (curr_direction_ == 90) {
        return "North";
    }
    else if (curr_direction_ < 180) {
        return "Northwest";
    }
    else if (curr_direction_ == 180) {
        return "West";
    }
    else if (curr_direction_ < 270) {
        return "Southwest";
    }
    else if (curr_direction_ == 270) {
        return "South";
    }
    else if (curr_direction_ < 360) {
        return "Southeast";
    }
    else {
        return "Out of bounds";
    }
}

/**
    @return: brand
**/
std::string Motorcycle::getBikeType() {
    switch(brand_) {
        case bike_details::bike_brand::YAMAHA: return "YAMAHA";
        case bike_details::bike_brand::DUCATI: return "DUCATI";
        case bike_details::bike_brand::HARLEY_DAVIDSON: return "HARLEY_DAVIDSON";
        case bike_details::bike_brand::KAWASAKI: return "KAWASAKI";
    }
}

/**
    @return: curr_speed_
**/
float Motorcycle::getSpeed() {
    return curr_speed_;
}

/**
    @return: distance_traveled_
**/
float Motorcycle::getDistanceTraveled() {
    return distance_traveled_;
}

/**
    @return: curr_acceleration_
**/
int Motorcycle::getIntensity() {
   return curr_acceleration_;
}

/**
    updates direction_
    @param degrees: -360 <= degrees <= 360, and if the user enters a number outside of these
                    bounds adjust @param degrees to be within this range
    */
void Motorcycle::turn(float degrees) {
    /*
    // bring degrees with range
    while ( degrees < -360 || degrees > 360) {
        if (degrees < -360) {
            degrees += 360;
        }
        else {
            degrees -= 360;
        }
    }

    curr_direction_ = curr_direction_ + degrees;
    // bring curr_direction_ with range
    while ( curr_direction_ <= -360 || curr_direction_ >= 360) {
        if (curr_direction_ <= -360) {
            curr_direction_ += 360;
        }
        else {
            curr_direction_ -= 360;
        }
    }
    */
    curr_direction_ = curr_direction_ + degrees;
}

/**
    alters curr_speed_ depending on curr_speed_, curr_acceleration_, and brand_

    USE THE FOLLOWING FORMULA: [ (acceleration) / 8 ] + [ (brand) * 17.64 ]
*/
void Motorcycle::updateSpeed() {
    curr_speed_ = (curr_acceleration_ / 8)  + (brand_ * 17.64);
}

/**
    if the current acceleration is not HIGH increase it by one level and call updateSpeed()
*/
void Motorcycle::accelerate() {
    // compiler complains if I don't account for all possible options in enum
    switch (curr_acceleration_) {
        case bike_details::NONE:
            curr_acceleration_ = bike_details::LOW;
            break;
        case bike_details::LOW:
            curr_acceleration_ = bike_details::MEDIUM;
            break;
        case bike_details::MEDIUM:
            curr_acceleration_ = bike_details::HIGH;
            break;
        case bike_details::HIGH:
            curr_acceleration_ = bike_details::HIGH;
            break;
    }
    updateSpeed();
}

/**
    if the current acceleration is not NONE decrease it by one level and call updateSpeed()
*/
void Motorcycle::brake() {
    // compiler complains if I don't account for all possible options in enum
    //cout << "Current Acceleration: " << curr_acceleration_ << endl;
    switch (curr_acceleration_) {
        case bike_details::NONE:
            curr_acceleration_ = bike_details::NONE;
            break;
        case bike_details::LOW:
            curr_acceleration_ = bike_details::NONE;
            break;
        case bike_details::MEDIUM:
            curr_acceleration_ = bike_details::LOW;
            break;
        case bike_details::HIGH:
            curr_acceleration_ = bike_details::MEDIUM;
            break;
    }
    updateSpeed();
}

/**
    given curr_speed_, curr_acceleration_, brand_, and @param float
    duration, calculate the distance traveled during the
    specified time; increment distance_traveled_ by this amount
    @param float duration: time traveled
    @return: updated distance_traveled_
*/
float Motorcycle::ride(float duration) {
    if (duration <= 0) {
        return 0;
    }
    distance_traveled_ += curr_speed_ * duration;

    /*
    cout << "Distance traveled is increasing by " << curr_speed_ * curr_acceleration_ * brand_ * duration << endl;
    cout << "curr_speed_: " << curr_speed_ << endl;
    cout << "Curr Accel: " << curr_acceleration_ << endl;
    cout << "brand: " << brand_ << endl;
    cout << "duration: " << duration << endl;
     */
    //return distance_traveled_;
    return curr_speed_ * duration;
}