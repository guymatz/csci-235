#include <iostream>
#include "Motorcycle.hpp"
using namespace std;

int main() {

    Motorcycle biker = Motorcycle();
    cout << "biker " << biker.getBikeType() << endl;

    Motorcycle by = Motorcycle(bike_details::bike_brand::YAMAHA);
    Motorcycle bd = Motorcycle(bike_details::bike_brand::DUCATI);
    Motorcycle bh = Motorcycle(bike_details::bike_brand::HARLEY_DAVIDSON);
    Motorcycle bk = Motorcycle(bike_details::bike_brand::KAWASAKI);

    biker.turn(0);
    by.turn(0);
    bd.turn(50.31);
    bh.turn(-2);
    bk.turn(-6000);

    cout << biker.getCurrDir() << ", " << biker.getDirection() << endl;
    cout << by.getCurrDir() << ", " << by.getDirection() << endl;
    cout << bd.getCurrDir() << ", " << bd.getDirection() << endl;
    cout << bh.getCurrDir() << ", " << bh.getDirection() << endl;
    cout << bk.getCurrDir() << ", " << bk.getDirection() << endl;

    biker.turn(0);
    cout << biker.getCurrDir() << ", " << biker.getDirection() << endl;
    biker.turn(45);
    cout << biker.getCurrDir() << ", " << biker.getDirection() << endl;
    biker.turn(45);
    cout << biker.getCurrDir() << ", " << biker.getDirection() << endl;
    biker.turn(45);
    cout << biker.getCurrDir() << ", " << biker.getDirection() << endl;
    biker.turn(45);
    cout << biker.getCurrDir() << ", " << biker.getDirection() << endl;
    biker.turn(45);
    cout << biker.getCurrDir() << ", " << biker.getDirection() << endl;
    biker.turn(45);
    cout << biker.getCurrDir() << ", " << biker.getDirection() << endl;
    biker.turn(45.0);
    cout << biker.getCurrDir() << ", " << biker.getDirection() << endl;
    biker.turn(45.0);
    cout << biker.getCurrDir() << ", " << biker.getDirection() << endl;
    biker.turn(45.0);
    cout << biker.getCurrDir() << ", " << biker.getDirection() << endl;

    for (int i = 0; i < 4; i=i+1) {
        by.accelerate();
        bd.accelerate();
        bh.accelerate();
        bk.accelerate();
    }

    cout << "by type: " << by.getBikeType() << ", " << by.getSpeed() << ", " << by.getIntensity() << endl;
    by.brake();
    cout << "by type: " << by.getBikeType() << ", " << by.getSpeed() << ", " << by.getIntensity() << endl;
    by.brake();
    cout << "by type: " << by.getBikeType() << ", " << by.getSpeed() << ", " << by.getIntensity() << endl;
    by.brake();
    cout << "by type: " << by.getBikeType() << ", " << by.getSpeed() << ", " << by.getIntensity() << endl;

    cout << "bd type: " << bd.getBikeType() << ", " << bd.getSpeed() << ", " << bd.getIntensity() << endl;
    bd.brake();
    cout << "bd type: " << bd.getBikeType() << ", " << bd.getSpeed() << ", " << bd.getIntensity() << endl;
    bd.brake();
    cout << "bd type: " << bd.getBikeType() << ", " << bd.getSpeed() << ", " << bd.getIntensity() << endl;
    bd.brake();
    cout << "bd type: " << bd.getBikeType() << ", " << bd.getSpeed() << ", " << bd.getIntensity() << endl;

    cout << "bh type: " << bh.getBikeType() << ", " << bh.getSpeed() << ", " << bh.getIntensity() << endl;
    bh.brake();
    cout << "bh type: " << bh.getBikeType() << ", " << bh.getSpeed() << ", " << bh.getIntensity() << endl;
    bh.brake();
    cout << "bh type: " << bh.getBikeType() << ", " << bh.getSpeed() << ", " << bh.getIntensity() << endl;
    bh.brake();
    cout << "bh type: " << bh.getBikeType() << ", " << bh.getSpeed() << ", " << bh.getIntensity() << endl;

    cout << "bk type: " << bk.getBikeType() << ", " << bk.getSpeed() << ", " << bk.getIntensity() << endl;
    bk.brake();
    cout << "bk type: " << bk.getBikeType() << ", " << bk.getSpeed() << ", " << bk.getIntensity() << endl;
    bk.brake();
    cout << "bk type: " << bk.getBikeType() << ", " << bk.getSpeed() << ", " << bk.getIntensity() << endl;
    bk.brake();
    cout << "bk type: " << bk.getBikeType() << ", " << bk.getSpeed() << ", " << bk.getIntensity() << endl;

    for (int i = 0; i < 4; i=i+1) {
        bk.accelerate();
        bd.accelerate();
        bh.accelerate();
        bk.accelerate();
    }

    bk.ride(0);
    cout << "You have traveled " << bk.getDistanceTraveled() << " in 0 mins" << endl;
    bk.ride(-69);
    cout << "You have traveled " << bk.getDistanceTraveled() << " in -69 mins" << endl;
    bk.ride(9001);
    cout << "You have traveled " << bk.getDistanceTraveled() << " in 9001 mins" << endl;
    bk.ride(1);
    cout << "You have traveled " << bk.getDistanceTraveled() << " in 1 mins" << endl;

    return 0;
}
