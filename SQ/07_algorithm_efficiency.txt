-Using Big O notation, indicate the worst case time
requirement of each of the following tasks.
  -Computing the sum of the first n even integers by using a for loop
    O(n)
  -Displaying all n integers in an array
    O(n)
  -Displaying all n integers in a sorted linked chain
    O(n)
  -Displaying all items in n linked chains of size n each
    O(n^2)
  -Displaying one array element
    O(1)
  -Displaying the last integer in a linked chain
    O(n)
  -Searching an array for one particular value
    O(n)
  -Searching a sorted array for one particular value
    depends which algorithm
  -Adding an item to a stack of n items
    O(1)
  -Adding an item to a bag of n items
    Depends if the bad is sorted, etc

-What is the Big O run time for the following algorithm? Justify your answer.

Assume that the operations that are not shown are independent of n.
for (int pass = 1; pass <= n; pass++){
    for (int index = 0; index < n; index++){
        for (int count = 1; count < 10; count++){
            //operations here independent of n
        } //end for
    }//end for
}//end for
-Consider an array of length n containing positive and negative integers in
random order.
Write C++ code that rearranges the integers so that the negative integers
appear before the positive integers. Your solution should be O(n).
-Prove that T(n) = 25n+14 is O(n) (i.e. find n0 and k such that,
for all n >= n0 25n+14>= kn)
-Which of the following expressions correctly describe T (n) = n log 2(n)?
Circle all that apply:
a)  O(n)
b)  Ω (n)
c)  Θ (n2)
d)  O(n2)
e)  Ω (n2) 
