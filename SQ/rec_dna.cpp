#include <stdlib.h>
#include <iostream>

bool is_complement(char a, char b) {
    std::cout << a << " " << b << std::endl;
    if ((a == 'T' and b == 'A') or (a == 'A' and b == 'T') 
            or
            (a == 'C' and b == 'G') or (a == 'G' and b == 'C')) {
        return true;
    }
    else {
        return false;
    }
}

bool rec_dna(std::string dna) {
    if (dna.size() == 0) {
        return true;
    }
    else {
        std::cout << "dna = " << dna << std::endl;
        return is_complement(dna[0], (dna[dna.size()-1]))
               and
               rec_dna(dna.substr(1, dna.size()-2));
    }

}

int main() {
    std::string dna1 = "TGCAACGCGTTGCA";
    std::string dna2 = "CAACGCGTTGCA";
    if (rec_dna(dna1)) {
        std::cout << dna1 << " is a palindrome" << std::endl;
    }
    else {
        std::cout << dna1 << " is NOT a palindrome" << std::endl;
    }

    if (rec_dna(dna2)) {
        std::cout << dna2 << " is a palindrome" << std::endl;
    }
    else {
        std::cout << dna2 << " is NOT a palindrome" << std::endl;
    }
}
