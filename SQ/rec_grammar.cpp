#include <stdlib.h>
#include <iostream>

bool rec_grammar(std::string s) {
    if (s.size() == 0) {
        return true;
    }
    else {
        std::cout << "s = " << s << std::endl;
        return rec_grammar(s.substr(1, s.size()-2));
    }

}

int main() {
    std::string s1 = "AAAABBB";
    std::string s2 = "AAAA";
    std::string s3 = "BBB";
    std::string s4 = "AACBB";
    if (rec_grammar(s1)) {
        std::cout << s1 << " is in grammar" << std::endl;
    }
    else {
        std::cout << s1 << " is NOT in grammar" << std::endl;
    }

    if (rec_grammar(s2)) {
        std::cout << s2 << " is in grammar" << std::endl;
    }
    else {
        std::cout << s2 << " is NOT in grammar" << std::endl;
    }

    if (rec_grammar(s3)) {
        std::cout << s3 << " is in grammar" << std::endl;
    }
    else {
        std::cout << s3 << " is NOT in grammar" << std::endl;
    }

    if (rec_grammar(s4)) {
        std::cout << s4 << " is in grammar" << std::endl;
    }
    else {
        std::cout << s4 << " is NOT in grammar" << std::endl;
    }
}
