//
// Created by Guy Matz on 2/14/21.
//

#ifndef PROJECT_01_TRUCK_HPP
#define PROJECT_01_TRUCK_HPP

#include "Vehicle.hpp"

class Truck : public Vehicle {
public:
    Truck(std::string name, std::string manufacturer, double top_speed, double weight, double mpg, double cargo_capacity);

    bool add_cargo(double weight_of_cargo);
    bool clear();
    double getCargoCapacity() const;
    double getHeldCargoWeight() const;
    void operator=(const Truck &t);
private:
    double cargo_capacity_;
    double weight_of_held_cargo_;
};


#endif //PROJECT_01_TRUCK_HPP
