//
// Created by Guy Matz on 2/11/21.
//
#ifndef PROJECT_01_CAR_HPP
#define PROJECT_01_CAR_HPP

#include <iostream>
#include <string>
#include "Vehicle.hpp"

class Car: public Vehicle {
public:
    Car(std::string name, std::string manufacturer, double top_speed, double weight, double mpg);
};
#endif //PROJECT_01_CAR_HPP