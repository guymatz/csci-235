//
// Created by Guy Matz on 2/13/21.
//

#ifndef PROJECT_01_BUS_HPP
#define PROJECT_01_BUS_HPP
#include <iostream>
#include "Vehicle.hpp"
//using namespace std;

class Bus : public Vehicle {
private:
    size_t num_seats_;
public:
    Bus(std::string name, std::string manufacturer, double top_speed, double weight, double mpg, size_t number_seats);
    size_t getNumSeats() const;
};


#endif //PROJECT_01_BUS_HPP
