//
// Created by Guy Matz on 2/13/21.
//

#include <iostream>
#include "Bus.hpp"
#include "Vehicle.hpp"
using namespace std;

/**
      Calls the parameterized constructor Vehicle(...)
      Remember to set the number of seats.
      Uses setGarageSpaces function to set garage space to 4.
      Uses setNumWheels function to set wheels to 8.
   */
Bus::Bus(std::string name, std::string manufacturer, double top_speed, double weight, double mpg, size_t number_seats) {
    name_ = name;
    manufacturer_ = manufacturer;
    top_speed_ = top_speed;
    weight_ = weight;
    mpg_ = mpg;
    num_seats_ = number_seats;
    setGarageSpaces(4);
    setNumWheels(8);
}

/**
   returns the number of seats
*/
size_t Bus::getNumSeats() const {
    return num_seats_;
}

