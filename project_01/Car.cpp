//
// Created by Guy Matz on 2/11/21.
//

#include "Car.hpp"
using namespace std;

/**
      Calls the parameterized constructor Vehicle(...)
      Uses setGarageSpaces function to set garage space to 2.
      Uses setNumWheels function to set wheels to 4.
*/
Car::Car(std::string name, std::string manufacturer, double top_speed, double weight, double mpg) {
    name_ = name;
    manufacturer_ = manufacturer;
    top_speed_ = top_speed;
    weight_ = weight;
    mpg_ = mpg;

    setGarageSpaces(2);
    setNumWheels(4);
    //std::cout << "set garage spaces to " << getGaragePositions() << std::endl;
    //std::cout << "set garage spaces to " << getSpaces() << std::endl;
}