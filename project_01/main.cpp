#include <iostream>
#include "Vehicle.hpp"
#include "Car.hpp"
#include "Motorcycle.hpp"
#include "Bus.hpp"
#include "Truck.hpp"
#include "Garage.hpp"
using namespace std;

int main() {
    Car c = Car("poopy", "stoopy", 3, 4, 5);
    Motorcycle m = Motorcycle("foo", "bar", 3, 4, 7);
    size_t num_seats = 15;
    Bus b = Bus("foo", "bar", 3, 4, 7, num_seats);
    cout << "Seats on the bus : " << b.getNumSeats() << endl;
    size_t inital_weight = 500;
    Truck t = Truck("twuck", "poopy", 5, inital_weight, 3, 2);
    t.add_cargo(inital_weight);
    cout << "t: " << t.getManufacturer() << " " << t.getName() << " " << t.getWeight() << " " << t.getHeldCargoWeight() << endl;
    t.clear();
    cout << "t: " << t.getManufacturer() << " " << t.getName() << " " << t.getWeight() << " " << t.getHeldCargoWeight() << endl;
    cout << "T's Carcgo Capacity: " << t.getCargoCapacity() << endl;
    size_t cap = 0;
    Garage g0 = Garage(cap);
    if (g0.addVehicle(t)) {
        cout << "truck was added to the empty garage" << endl;
    }
    else {
        cout << "truck was NOT added to the empty garage" << endl;
    }
    g0.display();

    Garage g = Garage();
    if (g.addVehicle(t)) {
        cout << "truck was added to the non-empty garage" << endl;
    }
    else {
        cout << "truck was NOT added to the non-empty garage" << endl;
    }
    g.display();

    if (g.addVehicle(c)) {
        cout << "car was added to the non-empty garage" << endl;
    }
    else {
        cout << "car was NOT added to the non-empty garage" << endl;
    }
    g.display();

    if (g.addVehicle(t)) {
        cout << "truck was added to the non-empty garage" << endl;
    }
    else {
        cout << "truck was NOT added to the non-empty garage" << endl;
    }
    g.display();


    if (g.addVehicle(c)) {
        cout << "car was added to the non-empty garage" << endl;
    }
    else {
        cout << "car was NOT added to the non-empty garage" << endl;
    }
    g.display();

    if (g.removeVehicle(t)) {
        cout << "truck was removed from the non-empty garage" << endl;
    }
    else {
        cout << "truck was NOT removed from the non-empty garage" << endl;
    }
    g.display();

    g.swapVehicles(c,t);
    g.display();

    Garage g2 = Garage(12);
    Car c1 = Car("Camry", "Toyota", 3, 4, 5);
    Car c2 = Car("Aventador", "Lamborghini", 3, 4, 5);
    Bus b1 = Bus("XD60", "Xcelsior", 3, 4, 5, 6);
    Motorcycle m1 = Motorcycle("Z900", "KAWASAKI", 3, 4, 7);
    Truck t1 = Truck("T680", "KENWORTH", 5, inital_weight, 3, 2);
    g2.addVehicle(c1);
    g2.addVehicle(b1);
    g2.swapVehicles(c2, c1);
    std::cout << "Display: " << std::endl;
    g2.display();
    g2.swapVehicles(m1, b1);
    std::cout << "Display: " << std::endl;
    g2.display();
    if (! g2.swapVehicles(t1, c2)) {
        std::cout << "didn't work" << std::endl;
    }
    std::cout << "Display: " << std::endl;
    g2.display();

    return 0;
}