//
// Created by Guy Matz on 2/14/21.
//

#include "Truck.hpp"
using namespace std;

/**
      Calls the parameterized constructor Vehicle(...)
      Remember to set the cargo capacity.
      Uses setGarageSpaces function to set garage space to 8.
      Uses setNumWheels function to set wheels to 18.
   */
Truck::Truck(std::string name, std::string manufacturer, double top_speed, double weight, double mpg, double cargo_capacity) {
    name_ = name;
    manufacturer_ = manufacturer;
    top_speed_ = top_speed;
    weight_ = weight;
    mpg_ = mpg;
    cargo_capacity_ = cargo_capacity;
    weight_of_held_cargo_ = 0;
    setGarageSpaces(8);
    setNumWheels(18);
}

/**
   Add the weight of the cargo the current weight_of_held_cargo_ ONLY if it does not
   exceed the capacity.
   Return true if you manage to add the cargo successfully, otherwise return false.
*/
bool Truck::add_cargo(double weight_of_cargo) {
    //std::cout << "woc: " << weight_of_cargo << ", w_" << weight_ << ", cc_" << cargo_capacity_ << endl;
    if (weight_of_cargo + weight_of_held_cargo_ > cargo_capacity_) {
        return false;
    }
    else {
        weight_of_held_cargo_ = weight_of_held_cargo_ + weight_of_cargo;
        return true;
    }
}

/**
   If weight_of_held_cargo_ isn't 0 then set it to 0, return true if you manage to clear it or else return false!
*/
bool Truck::clear() {
    if (weight_of_held_cargo_ > 0) {
        weight_of_held_cargo_ = 0;
        return true;
    }
    return false;
}

/**
   return the cargo_capacity_ variable
*/
double Truck::getCargoCapacity() const {
    //std::cout << "in gcc" << cargo_capacity_ << endl;
    return cargo_capacity_;
}

/**
   returns the weight_of_held_cargo_ variable
*/
double Truck::getHeldCargoWeight() const {
    return weight_of_held_cargo_;
}

/* assignment operator overload */
void Truck::operator=(const Truck &rhs)
{
    setGarageSpaces(8);
    setNumWheels(18);
    //std::cout << " M I HERE?!?1/ " << name_ << std::endl;
}