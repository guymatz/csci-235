//
// Created by Guy Matz on 2/11/21.
//

#include "Motorcycle.hpp"
#include "Vehicle.hpp"
using namespace std;

/**
      Calls the parameterized constructor Vehicle(...)
      Uses setGarageSpaces function to set garage space to 1.
      Uses setNumWheels function to set wheels to 2.
   */
Motorcycle::Motorcycle(std::string name, std::string manufacturer, double top_speed, double weight, double mpg) {
    name_ = name;
    manufacturer_ = manufacturer;
    top_speed_ = top_speed;
    weight_ = weight;
    mpg_ = mpg;
    setGarageSpaces(1);
    setNumWheels(2);
}