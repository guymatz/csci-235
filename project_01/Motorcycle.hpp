//
// Created by Guy Matz on 2/11/21.
//

#ifndef PROJECT_01_MOTORCYCLE_HPP
#define PROJECT_01_MOTORCYCLE_HPP

#include "Vehicle.hpp"

class Motorcycle: public Vehicle {
public:
    Motorcycle(std::string name, std::string manufacturer, double top_speed, double weight, double mpg);
};

#endif //PROJECT_01_MOTORCYCLE_HPP
