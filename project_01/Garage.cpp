#include "Garage.hpp"
#include "cmath"

namespace null
{
    Vehicle v;
}

/* Parameterized constructor with a default argument for capacity of 12 */
Garage::Garage(size_t capacity) {
    /* the number of vehicle slots in the caller */
    capacity_ = capacity;
    /* the number of occupied vehicle slots */
    num_vehicles_ = 0;

    /* a pointer to an array of vehicles - HINT:
     dynamically allocate the actual array */
    arr_ = new Vehicle[capacity];
    //Loop and put v in each spot.  Do I nee this?!
    for (int i=0; i < capacity_; i++) {
        arr_[i] = null::v;
    }
}

/**
      Inserts @param to_add into the number of spaces that correspond to its type
      into arr_[]; must insert from front to back

      As an example, if we had a garage that already contains an Audio R8 (a car)
      the default garage would have an Audio R8 object in both its first and second indeces.

      MUST INCREMENT num_vehicles_
      MUST RETURN FALSE IF isFull()
      MUST RETURN FALSE IF num_vehicles_ + to_add.getSpaces() > capacity_
    */
bool Garage::addVehicle(Vehicle to_add) {
    //std::cout << "adding " << to_add.getName() << std::endl;
    if (isFull()) {
        //std::cout << "Dorry!!  Full!!!!" << std::endl;
        return false;
    }
    else if (num_vehicles_ + to_add.getSpaces() > capacity_) {
        //std::cout << "Sorry!!  Not enough spaces!!!!" << std::endl;
        //std::cout << "\t" << num_vehicles_ << " " << to_add.getSpaces() << " " << capacity_ << std::endl;
        return false;
    }
    for (int i=0; i < to_add.getSpaces(); i++) {
        arr_[num_vehicles_] = to_add;
        //increment the number of slots
        num_vehicles_++;
        //std::cout << "added " << to_add.getName() << std::endl;
    }
    arrange();
    return true;
}

/**
  Replaces any object in arr_ that == @param to_remove with null::v

  MUST RETURN FALSE IF num_vehicles_ == 0
  MUST DECREMENT num_vehicles_
  MUST CALL arrange() immediately before return statement
*/
bool Garage::removeVehicle(Vehicle to_remove) {
    //std::cout << to_remove.getName() << " has " << to_remove.getSpaces() << " spaces " << std::endl;
    bool resp = false;
    for (int i=0; i < capacity_; i++) {
        if (arr_[i] == to_remove) {
            arr_[i] = null::v;
            //decrement the number of slots
            num_vehicles_--;
            resp = true;
        }
    }
    arrange();
    return resp;
}

/**
 * Return true when Garage is full
 * @return boolean
 */
bool Garage::isFull() const {
    if (num_vehicles_ == capacity_) {
        return true;
    }
    else {
        return false;
    }
}

/**
    Changes the contents of arr_ to have all null vehicles strictly at the end of the
    array and all inserted vehicles at the beginning of the array.
    HINT: create a new array and repoint arr_; don't do this "in-place"
*/
void Garage::arrange() {

    //std::cout << "I arrange things" << std::endl;
    Vehicle *new_arr = new Vehicle[capacity_];

    // next spot to use in new array
    size_t first_free_slot = 0;
    // loop through the original array, looking for slot with non-null cars in them
    for (int i=0; i < capacity_; i++) {
        //std::cout << "car " << arr_[i].getName() << " in slot " << i << " ... " << std::endl;
        //std::cout << "car " << v.getName() << " in slot " << i << " has spaces " << v.getSpaces() << std::endl;
        //std::cout << "\t" << v.getManufacturer() << std::endl;
        //std::cout << "\t" << v.getNumWheels() << std::endl;
        if (arr_[i].getName() != "") {
            //std::cout << "Putting auto " << v.getName() << " into slot " << first_free_slot << std::endl;
            new_arr[first_free_slot] = arr_[i];
            //  move the first free slot
            first_free_slot++;
        }
    }
    arr_ = new_arr;
}

/**
      Outputs the contents of the caller such that the manufacturer and vehicle name are
      only printed once per vehicle in arr_; for example if a garage object contains the
      aforementioned Audi R8 a call to display() will print

      Audi R8

      even though arr_ contains the vehicle in two positions - also print each vehicle on
      its own line

    */
void Garage::display() const {
   //std::cout << "Garabage capacity: " << capacity_ << ", current: " << num_vehicles_ << std::endl;
    Vehicle automobile = null::v;
    // start by comparing the vehicles in the garage to a null vehicle
    for (int i=0; i < capacity_; i++) {
        // if the vehicle changes and is not a null vehicle then we . . .
        if (automobile != arr_[i] && arr_[i].getName() != "") {
            //std::cout << arr_[i].getManufacturer() << " " << arr_[i].getName() << " in " << i << std::endl;
            std::cout << arr_[i].getManufacturer() << " " << arr_[i].getName() << std::endl;
            // change the vehicle so we don't keep mentioning it
            automobile = arr_[i];
        }
    }
}

/**
  HINT: use what you already have!
*/
bool Garage::swapVehicles(Vehicle swap_in, Vehicle swap_out) {
    //std::cout << "Removing " << swap_out.getName() << ", adding " << swap_in.getName() << std::endl;
    removeVehicle(swap_out);
    addVehicle(swap_in);
    return true;
}
