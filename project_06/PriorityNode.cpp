/****************************************************************************************************************************
Title        : PriorityNode.cpp
Auhor        : Guy Matz
Description  : implementation file of a Node class for a Queue
Dependencies : PriorityNode.hpp
****************************************************************************************************************************/
#include "PriorityNode.hpp"

/* Default Constructor */
template <class ItemType>
PriorityNode<ItemType>::PriorityNode() : next_{nullptr},priority_{0}
{
}

/* Parameterized Constructor accepting just the item*/
template <class ItemType>
PriorityNode<ItemType>::PriorityNode(const ItemType &anItem) : item_{anItem},next_{nullptr},priority_{0}
{
}

/* Parameterized Constructor accepting item and priority */
template <class ItemType>
PriorityNode<ItemType>::PriorityNode(const ItemType &anItem, const int priority) : item_{anItem},priority_{priority},next_{nullptr}
{
}

/* Parameterized Constructor accepting item, priority, and pointer to next node */
template <class ItemType>
PriorityNode<ItemType>::PriorityNode(const ItemType &anItem, const int priority, PriorityNode<ItemType>* next_node_ptr) : item_{anItem}, priority_{priority}, next_{next_node_ptr}
{
}

/* Setter: item_ */
template <class ItemType>
void PriorityNode<ItemType>::setItem(const ItemType &anItem)
{
    item_ = anItem;
}

/* Setter: priority_ */
template <class ItemType>
void PriorityNode<ItemType>::setPriority(int priority)
{
    priority_ = priority;
}

/* Setter: next_ */
template <class ItemType>
void PriorityNode<ItemType>::setNext(PriorityNode<ItemType> *next_node_ptr)
{
    next_ = next_node_ptr;
}

/* Accessor: item_ */
template <class ItemType>
ItemType PriorityNode<ItemType>::getItem() const
{
    return item_;
}

/* Accessor: next_ */
template <class ItemType>
PriorityNode<ItemType> *PriorityNode<ItemType>::getNext() const
{
    return next_;
}

/* Accessor: prev_ */
template <class ItemType>
int PriorityNode<ItemType>::getPriority() const
{
    return priority_;
}