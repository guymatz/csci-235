#include <iostream>
//#include "PriorityNode.hpp"
#include "PriorityQueue.hpp"

int main() {

    std::cout << "Hello, World!" << std::endl;
    //PriorityNode<int> n(5);
    //std::cout << n.getItem() << " " << n.getPriority() << std::endl;
    PriorityQueue<int> queue;
    std::cout << "enqueing items" << std::endl;
    std::cout << "displaying items" << std::endl;
    //queue.display();
    queue.enqueue(5, 5);
    std::cout << "displaying items" << std::endl;
    //queue.display();
    queue.enqueue(12, 4);
    std::cout << "displaying items" << std::endl;
    //queue.display();
    queue.enqueue(15, 10);
    std::cout << "displaying items" << std::endl;
    //queue.display();
    queue.enqueue(2, 2);
    std::cout << "displaying items 2" << std::endl;
    //queue.display();
    queue.enqueue(25, 9);
    std::cout << "displaying items 1" << std::endl;
    //queue.display();
    std::cout << "do I get here? " << queue.size() << std::endl;
    PriorityQueue<int> npq = PriorityQueue<int>(queue);
    while  (queue.size() >= 1) {
        std::cout << queue.front() << " " << queue.size() << std::endl;
        queue.dequeue();
    }
    std::cout << "displaying " << npq.size() << " items" << std::endl;
    while  (npq.size() >= 1) {
        std::cout << npq.front() << std::endl;
        npq.dequeue();
    }

    PriorityQueue<std::string> spq;
    spq.enqueue("had", 1);
    spq.enqueue("semester!", 3);
    spq.enqueue("great", 2);
    spq.enqueue("Hope", 0);
    spq.enqueue("you", 0);
    spq.enqueue("a", 1);
    std::cout << "Copying " << std::endl;
    PriorityQueue<std::string> nspq = PriorityQueue<std::string>(spq);
    while  (spq.size() >= 1) {
        std::cout << spq.front() << std::endl;
        spq.dequeue();
    }
    std::cout << "am I?" << std::endl;
    while  (nspq.size() >= 1) {
        std::cout << nspq.front() << std::endl;
        nspq.dequeue();
    }
    return 0;
}
