/****************************************************************************************************************************
Title        : PriorityQueue.cpp
Auhor        : Guy Matz
Description  : implementation file of a Priority Queue
Dependencies : PriorityQueue.hpp, PriorityNode.cpp, PriorityNode.hpp
****************************************************************************************************************************/

#include <iostream>
#include <cstdlib>

/* Default Constructor */
template <class ItemType>
PriorityQueue<ItemType>::PriorityQueue() : back_{nullptr}, front_{nullptr}, item_count{0}
{
}

/* Copy Constructor */
template <class ItemType>
PriorityQueue<ItemType>::PriorityQueue(const PriorityQueue &a_priority_queue) : back_{nullptr}, front_{nullptr}, item_count{0}
{
    // temporary priority node
    PriorityNode<ItemType>* tpn = a_priority_queue.getFrontPtr();
    if (!a_priority_queue.isEmpty())
    {
        for (int i=1; i <= a_priority_queue.size(); i++) {
            //std::cout << "Adding " << a_priority_queue.size() << std::endl;
            //std::cout << "Checking " << tpn << std::endl;
            ItemType item = tpn->getItem();
            int priority = tpn->getPriority();
            //std::cout << "Adding " << item << " " << priority << std::endl;
            enqueue(item, priority);
            tpn = tpn->getNext();
            //std::cout << "size of queue " << a_priority_queue.size() << " and " << size() << std::endl;
        }
    }
}

/* Destructor */
template <class ItemType>
PriorityQueue<ItemType>::~PriorityQueue()
{
    // handles edge case of empty list
    if (item_count == 0)
    {
        return;
    }
    PriorityNode<ItemType>* node = getFrontPtr();
    for (int i=1; i <= item_count; i++) {
        ////std::cout << node->getItem() << std::endl;
        node = node->getNext();
    }
}

/* return item from first element in queue */
template <class ItemType>
ItemType PriorityQueue<ItemType>::front() const {
    return getFrontPtr()->getItem();
}

/* Accessor: item_count */
template <class ItemType>
int PriorityQueue<ItemType>::size() const
{
    return item_count;
}

/* Accessor: front_ */
template <class ItemType>
PriorityNode<ItemType> *PriorityQueue<ItemType>::getFrontPtr() const
{
    ////std::cout << "Going to return " << front_ << std::endl;
    return front_;
}

/**
    inserts parameter item into queue at position in queue specified by priority
    @param item - thing to be inserted in queue
    @param priority - place in queue
*/
template <class ItemType>
void PriorityQueue<ItemType>::enqueue(const ItemType &item, const int priority)
{
    // New node
    PriorityNode<ItemType>* new_node = new PriorityNode<ItemType>(item, priority);
    PriorityNode<ItemType>* curr_ptr = back_;

    //std::cout << "Enqueuing " << new_node << std::endl;
    if (item_count == 0) {
        // First enqueue
        //std::cout << "enqueue 1" << std::endl;
        back_ = new_node;
        front_ = new_node;
        new_node->setNext(nullptr);
        ////std::cout << "Current State: front_p=" << front_->getPriority() << ", back_p=" << back_->getPriority() << std::endl;
    }
    else if (priority < getFrontPtr()->getPriority()) {
        // New node has top priority
        //std::cout << "enqueue 2" << std::endl;
        //std::cout << "Current State: front_p=" << front_->getPriority() << ", back_p=" << back_->getPriority() << std::endl;
        curr_ptr = getFrontPtr();
        new_node->setNext(curr_ptr);
        front_ = new_node;
    }
    else if (priority >= back_->getPriority()){
        // New node has lowest priority
        //std::cout << "enqueue 3" << std::endl;
        //std::cout << "Current State: front_p=" << front_->getPriority() << ", back_p=" << back_->getPriority() << std::endl;
        curr_ptr->setNext(new_node);
        back_ = new_node;
        new_node->setNext(nullptr);
    }
    else {
        curr_ptr = front_;

        int ctr = 1;
        while (curr_ptr->getNext() != nullptr) {
            //std::cout << ctr++ << " " << curr_ptr->getPriority() << " " << priority << " " << curr_ptr->getNext()->getPriority() << std::endl;
            if (priority < curr_ptr->getNext()->getPriority()) {
                //std::cout << "HERE? enqueue 4, curr=" << curr_ptr->getPriority() << ", new=" << new_node->getPriority() << ", curr_next_p=" << curr_ptr->getNext()->getPriority() << std::endl;
                //std::cout << "HERE? enqueue 4, curr=" << curr_ptr << ", new=" << new_node << ", curr_next=" << curr_ptr->getNext() << std::endl;
                new_node->setNext(curr_ptr->getNext());
                curr_ptr->setNext(new_node);
                //std::cout << "HERE? enqueue 4, curr next=" << curr_ptr->getNext() << ", new next=" << new_node->getNext() << ", curr_next next=" << curr_ptr->getNext()->getNext() << std::endl;
                break;
            }
            curr_ptr = curr_ptr->getNext();
        }
    }
    item_count++;
} // end enqueue()

/**
    removes node from front of queue
*/
template <class ItemType>
void PriorityQueue<ItemType>::dequeue()
{
    PriorityNode<ItemType>* n = front_;
    front_ = n->getNext();
    delete n;
    n = nullptr;
    item_count--;
} // end dequeue()

/* Accessor: itemCount_ == 0 ? */
template <class ItemType>
bool PriorityQueue<ItemType>::isEmpty() const
{
    return (item_count == 0);
}