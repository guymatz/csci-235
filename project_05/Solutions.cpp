#include "DoublyLinkedList.hpp"
#include <functional>
#include <string>

namespace solutions
{
    /* Bubblesort */
    /** sorts the list based on comp using bubblesort algorithm
    @post  a_list[first..last] is sorted.
    @param a_list  The given DoublyLinkedList.
    @param comp  function to use for comparing objects
     */
    template <typename Comparable, typename Comparator>
    void bubbleSort(DoublyLinkedList<Comparable> &a_list, const Comparator &comp)
    {
        //std::cout << "Bubble sorting . . . "  << std::endl;
        for (int j=0; j < a_list.getSize(); j++) {
            int max_slot = a_list.getSize() - j;
            for (int i=1; i < max_slot; i++) {
                Comparable x = a_list.getAtPos(i)->getItem();
                Comparable y = a_list.getAtPos(i+1)->getItem();
                if (comp(y, x)) {
                    a_list.swap(i, i+1);
                }
            }
        }
    }

    /* Insertionsort */
    /** sorts the list based on comp using insertion sort algorithm
    @post  a_list[first..last] is sorted.
    @param a_list  The given DoublyLinkedList.
    @param comp  function to use for comparing objects
     */
    template <typename Comparable, typename Comparator>
    void insertionSort(DoublyLinkedList<Comparable> &a_list, const Comparator &comp)
    {
        //std::cout << "Insertion sorting . . . "  << a_list.getSize() << std::endl;
        for (int i=1; i <= a_list.getSize(); i++) {
            for (int j=i+1; j <= a_list.getSize(); j++) {
                Comparable x = a_list.getAtPos(i)->getItem();
                Comparable y = a_list.getAtPos(j)->getItem();
                if (comp(y, x)) {
                    //std::cout << "swapping  " << i << " and " << j << std::endl;
                    a_list.swap(i, j);
                }
            }
        }
    }

   /** Merges two sorted array segments theArray[first..mid] and
       theArray[mid+1..last] into one sorted array.
    @pre  first <= mid <= last. The sublists a_list[first..mid] and
       a_list[mid+1..last] are each sorted in increasing order.
    @post  a_list[first..last] is sorted.
    @param a_list  The given DoublyLinkedList.
    @param left_index  The index of the beginning of the first segment in
       a_list.
    @param middle_index  The index of the end of the first segment in a_list;
     mid + 1 marks the beginning of the second segment.
    @param right_index  The index of the last element in the second segment in a_list.
    @param comp  comparator function
   */
    template <typename Comparable, typename Comparator>
    void merge(DoublyLinkedList<Comparable> &a_list, int left_index, int middle_index, int right_index, const Comparator &comp)
    {
        int min_idx = left_index;
        //std::cout << "i = " << min_idx << ", l = " << left_index << ", m = " << middle_index << ", r = " << right_index << ": ";
        //a_list.display(left_index, right_index);
        //std::cout << std::endl;
        while (min_idx < right_index) {
            //std::cout << "i = " << min_idx << ", l = " << left_index << ", m = " << middle_index << ", r = " << right_index << std::endl;
            // right hand side is empty, copy left side
            if (middle_index > right_index) {
                //std::cout << "middle == right, m=" << middle_index << ", r=" << right_index  << std::endl;
                //a_list.display();
                return;
                for (int i=left_index; i <= middle_index; i++) {
                    //std::cout << "LEFT: Inserting " << a_list.getAtPos(i)->getItem() << " into slot " << min_idx << std::endl;
                    //std::cout << "i = " << min_idx << ", l = " << left_index << ", m = " << middle_index << ", r = " << right_index << std::endl;
                    a_list.insert(a_list.getAtPos(i)->getItem(), min_idx);
                    //a_list.display();
                    //std::cout << "Removing " << a_list.getAtPos(i)->getItem() << " from slot " << i << std::endl;
                    a_list.remove(i);
                    //a_list.display();
                    min_idx++;
                }
                // should be nothing more to do here
                return;
            }
            // left hand side is empty, copy right side
            else if (middle_index < left_index) {
                //std::cout << "middle == left, m=" << middle_index << ", l=" << left_index << std::endl;
                //a_list.display();
                return;
                for (int i=middle_index; i <= right_index; i++) {
                    //std::cout << "LEFT: Inserting " << a_list.getAtPos(i)->getItem() << " into slot " << min_idx << std::endl;
                    //std::cout << "i = " << min_idx << ", l = " << left_index << ", m = " << middle_index << ", r = " << right_index << std::endl;
                    a_list.insert(a_list.getAtPos(i)->getItem(), min_idx);
                    //a_list.display();
                    //std::cout << "Removing " << a_list.getAtPos(i)->getItem() << " from slot " << i << std::endl;
                    a_list.remove(i);
                    //a_list.display();
                    //std::cout << "Removed " << std::endl;
                    min_idx++;
                }
                // should be nothing more to do here
                return;
            }
            else {
                //std::cout << "Comparing "  << a_list.getAtPos(left_index)->getItem()  << " comp " << a_list.getAtPos(middle_index)->getItem() << std::endl;
                if (comp(a_list.getAtPos(left_index)->getItem(), a_list.getAtPos(middle_index)->getItem())) {
                    //std::cout << "LEFT: Inserting " << a_list.getAtPos(left_index)->getItem() << " into slot " << min_idx << std::endl;
                    a_list.insert(a_list.getAtPos(left_index)->getItem(), min_idx);
                    left_index++;
                    //middle_index++;
                    //std::cout << "Removing " << a_list.getAtPos(left_index)->getItem() << " from slot " << left_index << std::endl;
                    a_list.remove(left_index);
                }
                else {
                    //std::cout << "MIDDLE: Inserting " << a_list.getAtPos(middle_index)->getItem() << " into slot " << min_idx << std::endl;
                    a_list.insert(a_list.getAtPos(middle_index)->getItem(), min_idx);
                    //a_list.display();
                    left_index++;
                    middle_index++;
                    //std::cout << "Removing " << a_list.getAtPos(middle_index)->getItem() << " from slot " << middle_index << std::endl;
                    a_list.remove(middle_index);
                }
                //a_list.display();
                // move the min index one spot over
                min_idx++;
            }
        }
    }

   /** Sorts the items in an a_list into order defined by comp.
    @pre  a_list[first..last] is a double-linked list.
    @post  a_list[first..last] is sorted in order.
    @param a_list  The given DLL.
    @param left_index  The index of the first element to consider in a_list.
    @param right_index  The index of the last element to consider in a_list.
    @param comp  comparator function*/
    template <typename Comparable, typename Comparator>
    void mergeSort(DoublyLinkedList<Comparable> &a_list, int left_index, int right_index, const Comparator &comp)
    {
        //std::cout << "Merge Sorting with left/right. . . " << left_index << " " << right_index << std::endl;
        if (right_index - left_index == 0) {
            //std::cout << "Done" << std::endl;
            return;
        }
        int mid_index = left_index + (right_index - left_index)/2;
        mergeSort(a_list, left_index, mid_index, comp);
        mergeSort(a_list, mid_index+1, right_index, comp);

        //std::cout << std::endl << "Merging . . . "  << left_index << " " << mid_index+1 << " "  << right_index << std::endl;
        //a_list.display();
        merge(a_list, left_index, mid_index+1, right_index, comp);
    }

    /* Mergesort Wrapper */
    /** Sorts the items in an a_list into order defined by comp.
     @pre  a_list[first..last] is a double-linked list.
     @post  a_list[first..last] is sorted in order.
     @param a_list  The given DLL.
     @param comp  comparator function*/
    template <typename Comparable, typename Comparator>
    void mergeSort(DoublyLinkedList<Comparable> &a_list, const Comparator &comp)
    {
        //std::cout << "Merge Sorting . . . "  << std::endl;
        mergeSort(a_list, 1, a_list.getSize(), comp);
    }

    /** SOrts the the using the Shell sort algorithm
     *
     * @pre  a_list[first..last] is a double-linked list.
     * @tparam Comparable
     * @tparam Comparator
     * @param a_list is a doubly-linked list
     * @param comp is a comparator function
     */
    /* Shellsort */
    template <typename Comparable, typename Comparator>
    void shellSort(DoublyLinkedList<Comparable> &a_list, const Comparator &comp)
    {
        //std::cout << "Shell Sorting . . . "  << std::endl;
        // set up a gap to compare elements in the list
        int gap = a_list.getSize() / 2 - 1;
        while (gap > 0) {
            //std::cout << "gap = " << gap << ", len = " << a_list.getSize()  << std::endl;
            for (int i=1; i < (a_list.getSize() - gap + 1); i++) {
                //std::cout << i << gap << a_list.getSize() << std::endl;
                Comparable x = a_list.getAtPos(i)->getItem();
                Comparable y = a_list.getAtPos(i+gap)->getItem();
                //std::cout << "Comparing " << x << " "  << y << std::endl;
                if (comp(y, x)) {
                    //std::cout << "\tSwapping " << x << " "  << y << std::endl;
                    a_list.swap(i, i + gap);
                    //a_list.display();
                }
            }
            gap--;
        }

    }

    /* Quicksort */
    /** recursive function implementing the quick sort algorithm
     *
     * @tparam Comparable
     * @tparam Comparator
     * @param a_list - doubly-linked list to be sorted
     * @param left_index - left tindex to begin comparison
     * @param right_index - right tindex to begin comparison
     * @param comp - comparator function
     */
    template <typename Comparable, typename Comparator>
    void quickSort(DoublyLinkedList<Comparable> &a_list, int left_index, int right_index, const Comparator &comp)
    {
        if (right_index - left_index <= 1) {
            return;
        }
        //std::cout << "Quick Sorting . . . "  << left_index << " " << right_index << std::endl;
        //std::cout << "\t\t"  << a_list.getAtPos(left_index)->getItem() << "@" << left_index << " " << a_list.getAtPos(right_index)->getItem() << "@" << right_index << std::endl;

        // take not of pivot, left amd right.
        int pivot_index = left_index;
        int original_left_index = left_index;
        int original_right_index = right_index;
        Comparable pivot_val = a_list.getAtPos(pivot_index)->getItem();
        // move left past the pivot
        left_index++;
        while (left_index <= right_index) {
            //a_list.display();
            Comparable left_val = a_list.getAtPos(left_index)->getItem();
            Comparable right_val = a_list.getAtPos(right_index)->getItem();

            //std::cout << pivot_val << " " << left_val << " " << right_val << std::endl;
            if (comp(pivot_val, left_val) && comp(right_val, pivot_val)) {
                //std::cout << "Swapping, before: " << std::endl;
                //a_list.display();
                a_list.swap(left_index, right_index);
                //std::cout << "Swapping, after: " << std::endl;
                //a_list.display();
                left_index++;
                right_index--;
            }
            else if (comp(left_val, pivot_val)) {
                left_index++;
            }
            else if (comp(pivot_val, right_val)) {
                right_index--;
            }
        }
        //std::cout << "ol: " << original_left_index <<", l:" << left_index << ", or: " << original_right_index <<", r:" << right_index << std::endl;
        //std::cout << "inserting odd " << pivot_val << " to slot:" << left_index << std::endl;
        // move pivot to it's new spot and remove original
        a_list.insert(pivot_val, left_index);
        a_list.remove(pivot_index);
        //a_list.display();
        quickSort(a_list, original_left_index, left_index-1, comp);
        quickSort(a_list, right_index+1, original_right_index, comp);
    }

    /* Quicksort Wrapper */
    /** passes double-linked list to quickSort(a_list, int, int, comp) for sorting
     * @tparam Comparable
     * @tparam Comparator
     * @param a_list - douby-linked list to be sorted
     * @param comp  - comparison function
     */
    template <typename Comparable, typename Comparator>
    void quickSort(DoublyLinkedList<Comparable> &a_list, const Comparator &comp)
    {
        //std::cout << "Quick Sorting wrapper. . . "  << std::endl;
        quickSort(a_list, 1, a_list.getSize(), comp);
    }

}; // namespace solutions