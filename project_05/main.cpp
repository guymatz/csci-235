#include <iostream>
#include <functional>
//#include "DoubleNode.hpp"
#include "DoublyLinkedList.hpp"
#include "Solutions.cpp"

int main() {
    //std::cout << "Hello, World!" << std::endl;

    //std::cout << "List of strings" << std::endl;
    DoublyLinkedList<int> sb_dll;
    sb_dll.insert(10, 1);
    sb_dll.insert(16, 2);
    sb_dll.insert(8, 3);
    sb_dll.insert(12, 4);
    sb_dll.insert(15, 5);
    sb_dll.insert(6, 6);
    sb_dll.insert(3, 7);
    sb_dll.insert(9, 8);
    sb_dll.insert(25, 9);
    sb_dll.insert(13, 10);
    /*
    DoublyLinkedList<std::string> si_dll = sb_dll;
    sb_dll.display();
    std::cout << "Bubble sort" << std::endl;
    solutions::bubbleSort(sb_dll, std::greater<std::string>());
    sb_dll.display();
    std::cout << "Insertion sort" << std::endl;
    si_dll.display();
    solutions::insertionSort(si_dll, std::greater<std::string>());
    si_dll.display();
    */
    DoublyLinkedList<int> sm_dll = sb_dll;
    DoublyLinkedList<int> sq_dll = sb_dll;
    /*
    std::cout << "Merge sort" << std::endl;
    sm_dll.display();
    solutions::mergeSort(sm_dll, std::less<std::string>());
    sm_dll.display();
     */
    std::cout << "Shell sort" << std::endl;
    sq_dll.display();
    solutions::shellSort(sq_dll, std::greater<int>());
    sq_dll.display();

    /*
    std::cout << "List of ints" << std::endl;
    DoublyLinkedList<int> i_dll;
    i_dll.insert(1, 1);
    i_dll.insert(2, 1);
    i_dll.insert(3, 1);
    i_dll.insert(4, 2);
    i_dll.insert(5, 1);
    i_dll.insert(6, 3);
    i_dll.display();
    //solutions::bubbleSort(i_dll, std::greater<int>());
    i_dll.display();
    i_dll.clear();
    */

    return 0;
}
