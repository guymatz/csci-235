#include <iostream>
#include "Garage.hpp"
#include "Vehicle.hpp"
#include "Object.hpp"

int main() {
    Car c = Car("car", "CAR", 5, 4, 6);
    //Car *c1 = new Car("poopy", "stoopy", 5, 4, 6);
    Bus b = Bus("bus", "BUS", 5, 4, 6, 12);
    /*
    std::cout << "c name in main: " << c.getName() << std::endl;
    std::cout << "c manufacturer in main: " << c.getManufacturer() << std::endl;
    std::cout << "c name in main: " << c.getName() << std::endl;
    std::cout << "c name in main: " << &c << std::endl;
    */
    Object o = Object();
    Garage<Vehicle> gv;
    Garage<Object> go;
    gv.display();
    gv.add(c);
    gv.display();
    gv.add(b);
    gv.display();
    gv.clear();
    gv.display();
    if (gv.isEmpty()) {
        std::cout << "gv is empty" << std::endl;
    }
    if (go.isEmpty()) {
        std::cout << "go is empty" << std::endl;
    }
    gv.remove(c);
    if (gv.isEmpty()) {
        std::cout << "gv is empty" << std::endl;
    }
    gv.add(c);
    gv.swap(b, c);
    gv.add(c);
    gv.add(c);
    //gv.add(*c1);
    std::cout << "Freq of car 0: " << gv.getFrequencyOf(c) << std::endl;
    //std::cout << "Freq of car 1: " << gv.getFrequencyOf(*c1) << std::endl;
    std::cout << "Freq of bus: " << gv.getFrequencyOf(b) << std::endl;
    gv.display();
    std::cout <<" clear" << std::endl << std::endl;
    gv.clear();
    gv.display();
    gv.remove(b);
    gv.add(b);
    std::cout <<" add b" << std::endl << std::endl;
    gv.display();
    Truck t = Truck("truck", "Kenworth", 5, 6, 7, 8);
    gv.add(t);
    std::cout <<" add t" << std::endl << std::endl;
    gv.display();
    gv.remove(b);
    std::cout <<" remove b" << std::endl << std::endl;
    gv.display();
    gv.remove(t);
    std::cout <<" remove t" << std::endl << std::endl;
    gv.display();
    Toolbox tb = Toolbox("tool", "BOX", 5, 6);
    Lawnmower lm = Lawnmower("lawn", "MOWER", 5, 6);
    GardenHose gh = GardenHose("garden", "HOSE", 5, 6);
    go.add(tb);
    go.add(lm);
    Garage<Object> go1;
    go1.add(tb);
    go1.add(lm);
    go1.add(gh);
    go /= go1;
    std::cout << " in go" << std::endl;
    go.display();
    std::cout << " in go1" << std::endl;
    go1.display();
    gv.clear();
    std::cout << "Cleared gv: " << std::endl;
    gv.display();
    Car tc = Car("Camry", "Toyota", 5, 6, 7);
    Truck xx = Truck("XD60", "Xelcior", 5, 6, 7, 8);
    Motorcycle zk = Motorcycle("Z900", "Kawasaki", 5, 6, 7);
    gv.add(tc);
    gv.add(xx);
    gv.add(zk);
    std::cout << "Just added .. ." << std::endl;
    gv.display();

    std::vector<Vehicle> v = gv.toVector();
    return 0;
}
