//
// Created by Guy Matz on 3/3/21.
//
#include <iostream>
#include "Garage.hpp"

template <typename ItemType>
Garage<ItemType>::Garage(size_t capacity) {
    //std::cout << "I'm a garage!" << std::endl;
    spaces_occupied_ = 0;    // Current count of garage items
    capacity_ = capacity;            // Size of items_
    items_ = new ItemType[capacity_];           // Array of garage items
}

/**
    @return spaces_occupied_ : the number of vehicles currently stored in the caller
**/
template <typename ItemType>
size_t Garage<ItemType>::getCurrentSize() const {
    return spaces_occupied_;
}

/**
    @return true if spaces_occupied_ == 0, false otherwise
**/
template <typename ItemType>
bool Garage<ItemType>::isEmpty() const {
    //std::cout << "Spaces: " << spaces_occupied_ << std::endl;
    return spaces_occupied_ == 0;
}

/**
    @return true if spaces_occupied_ == capacity_, false otherwise
**/
template <typename ItemType>
bool Garage<ItemType>::isFull() const {
    return spaces_occupied_ == capacity_;
}

/**
    @return true if to_add was successfully added to items_, false otherwise
**/
template <typename ItemType>
bool Garage<ItemType>::add(const ItemType &to_add) {
    //std::cout << "Adding " << &to_add << std::endl;
    if (isFull()) {
        //std::cout << "Full .." << std::endl;
        return false;
    }
    for (int i=0; i < to_add.getSpaces(); i++) {
        items_[spaces_occupied_] = to_add;
        spaces_occupied_++;
    }
    //std::cout << "spaces occupied " << spaces_occupied_ <<  " " << to_add.getName() << std::endl;
    return true;
}

/**
        @return true if a single instance of an_entry was successfully removed from items_, false otherwise
**/
template <typename ItemType>
bool Garage<ItemType>::remove(const ItemType &to_remove) {
    bool removed = false;

    ItemType* new_arr = new ItemType[capacity_];  // NEW Array of garage items

    // loop through the list and copy and vehicles that are not to be removed to the
    // new list, then copy new list to old list!
    for (int i=0; i < capacity_; i++) {
        if (to_remove == items_[i]) {
            spaces_occupied_--;
            items_[i] = items_[spaces_occupied_];
            removed = true;
        }
        else {
            new_arr[i] = items_[i];
        }
    }
    items_ = new_arr;
    return removed;
}

/**
        @return true if you manage to swap the given items, false otherwise.
**/
template <typename ItemType>
bool Garage<ItemType>::swap(ItemType in, ItemType out) {
    if (remove(out) && add(in)) {
        return true;
    }
    return false;
}

/**
    @post spaces_occupied_ == 0
**/
template <typename ItemType>
void Garage<ItemType>::clear() {
    spaces_occupied_ = 0;
}

/**
        @return the number of times an_entry is found in items_
    **/
template <typename ItemType>
int Garage<ItemType>::getFrequencyOf(const ItemType &an_entry) const {
    int count = 0;
    for (int i=0; i < spaces_occupied_; i++) {
        if (items_[i] == an_entry) {
            //std::cout << "here " << &items_[i] << " " << &an_entry << std::endl;
            count++;
        }
    }
    return count;
}

/**
     @return a vector that has the same contents as items_
**/
template <typename ItemType>
std::vector<ItemType> Garage<ItemType>::toVector() const {

    std::vector<ItemType> v;
    // cycle through the list and concat the names, allowing us to compare
    // against what we've already seen
    std::string tmp = "";

    for (int i=0; i < spaces_occupied_; i++) {
        //std::cout << i << " " << tmp << " " << tmp2 << std::endl;
        if (tmp != (items_[i].getName() + items_[i].getManufacturer())) {
            //std::cout << "To Vector: " << items_[i].getName() << " " << items_[i].getManufacturer() << std::endl;
            v.push_back(items_[i]);
            tmp = items_[i].getName() + items_[i].getManufacturer();
        }
    }
    return v;
}

/**
        @return true if an_entry is found in items_, false otherwise
**/
template <typename ItemType>
bool Garage<ItemType>::contains(const ItemType &an_entry) const {
    for (int i=0; i < spaces_occupied_; i++) {
        if (items_[i] == an_entry) {
            return true;
        }
    }
    return false;
}

/**
      Outputs the names of the content in the Garage ADT.
      If there are multiple instances of an item in the Garage, you only output it once.

        Example Output:
                Item_1_Name Item_1_Manufacturer
                Item_2_Name Item_2_Manufacturer
                Item_3_Name Item_3_Manufacturer

**/
template <typename ItemType>
void Garage<ItemType>::display() const {

    // cycle through the list and concat the names, allowing us to compare
    // against what we've already seen
    std::string tmp = "";

    for (int i=0; i < spaces_occupied_; i++) {
        //std::cout << i << " " << tmp << " " << tmp2 << std::endl;
        if (tmp != (items_[i].getName() + items_[i].getManufacturer())) {
            //std::cout << items_[i].getName() << " ! " << items_[i].getManufacturer() <<
            //    " " << i <<  " " << tmp << " " << &items_[i] << std::endl;
            std::cout << items_[i].getName() << " " << items_[i].getManufacturer() << std::endl;
            tmp = items_[i].getName() + items_[i].getManufacturer();
        }
    }
}

/** implements Set Union over the caller and a_garage. The union of two sets A and B is the set of elements which are in A, in B, or in both A and B.
    @param a_garage to be combined with the contents of this (the calling) garage
    @post adds as many items from a_garage as space allows
*/
template <typename ItemType>
void Garage<ItemType>::operator+=(const Garage<ItemType> &a_garage) {
    for (int i=0; i < a_garage.spaces_occupied_; i++) {
        if (isFull()) {
            return;
        }
        add(a_garage.items_[i]);
    }
}

/** implements Set Difference over the caller and a_garage. The difference between two sets A and B is the set that consists of the elements of A which are not elements of B
    @param a_garage to be subtracted from this (the calling) garage
    @post removes all data from items_ that are also found in a_garage
*/
template <typename ItemType>
void Garage<ItemType>::operator-=(const Garage<ItemType> &a_garage) {
    for (int i=0; i < a_garage.spaces_occupied_; i++) {
        if (isEmpty()) {
            return;
        }
        remove(a_garage.items_[i]);
    }
}


/** implements Set Intersection over the caller and a_garage. The intersection of two
 * sets A and B is the set that consists of the elements that are in both A and B
    @param a_garage to be intersected with this (the calling) garage
    @post items_ no longer contains data that is not found in a_garage
*/
template <typename ItemType>
void Garage<ItemType>::operator/=(const Garage<ItemType> &a_garage) {

    ItemType* new_arr = new ItemType[capacity_];  // NEW Array of garage items
    size_t spaces = 0;

    // loop through the list and copy and vehicles that are not to be removed to the
    // new list, then copy new list to old list!
    for (int i=0; i < capacity_; i++) {
        if (a_garage.contains(items_[i])) {
            //std::cout << "Keeping " << items_[i].getName() << std::endl;
            new_arr[spaces] = items_[i];
            spaces++;
        }
    }
    spaces_occupied_ = spaces;
    items_ = new_arr;
}

/**
    @param target to be found in items_
    @return either the index target in the array items_ or -1,
    if the array does not contain the target.
**/
template <typename ItemType>
int Garage<ItemType>::getIndexOf(const ItemType &target) const {
    size_t rc = -1;
    for (int i=0; i < spaces_occupied_; i++) {
        if (items_[i] == target) {
            rc = i;
            break;
        }
    }
    return rc;
}
