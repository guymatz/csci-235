//
// Created by Guy Matz on 4/6/21.
//

#ifndef PROJECT_04_TRIANGLE_HPP
#define PROJECT_04_TRIANGLE_HPP

#include "Shape.hpp"

class Triangle : public Shape {
public:
    /* Parameterized constructor; takes in side length as a parameter, iterates
    through the 2D array to draw the right triangle using ASCII chars */
    Triangle(const int &side);

    double getSurfaceArea();
    double get3DVolume(const double &depth);
};


#endif //PROJECT_04_TRIANGLE_HPP
