//
// Created by Guy Matz on 4/6/21.
//

#ifndef PROJECT_04_CIRCLE_HPP
#define PROJECT_04_CIRCLE_HPP

#include "Shape.hpp"

class Circle : public Shape {
public:
    //Parameterized contructor: takes diameter as width or height.
    Circle(const int& diameter);

    virtual double getSurfaceArea() override;
    virtual double get3DVolume(const double& depth) override;
};


#endif //PROJECT_04_CIRCLE_HPP
