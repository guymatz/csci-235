//
// Created by Guy Matz on 4/6/21.
//

#include "Triangle.hpp"
#include <cmath>
#include <iostream>

/* Parameterized constructor; takes in side length as a parameter, iterates
    through the 2D array to draw the right triangle using ASCII chars */
/**
     Parameterized Constructor
     @param side   : height & width of the triangle
 */
Triangle::Triangle(const int &side) : Shape(side, side) {
    setEdges(3);

    // Populate 2D array with empty chars
    char **arr = new char *[getHeight()];
    for (int row = 0; row < getHeight(); row++)
    {
        arr[row] = new char[getWidth()];
        for (int col = 0; col < getWidth(); col++)
        {
            arr[row][col] = ' ';
        }
    }

    // Populate the proper positions with *'s
    char ascii_counter = 48;

    for (int row = 0; row < getHeight(); row++)
    {
        for (int col = 0; col < getWidth(); col++)
        {
            // we populate first column, last row & hypostenuse
            if (col == 0 or row == (getHeight()-1) or col == row)
            {
                arr[row][col] = ascii_counter;

                // fix ascii_counter to wrap around after
                ascii_counter++;
                if (ascii_counter > 126)
                {
                    ascii_counter = 48;
                }
            }
        }
    }
    setDisplayChars(arr);
    //std::cout << "I Just made a triangle" << std::endl;;
}

double Triangle::getSurfaceArea() {
    return 1/2.0 * getWidth() * getHeight();
}

double Triangle::get3DVolume(const double &depth) {
    return getSurfaceArea() * depth;
}
