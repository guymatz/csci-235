//
// Created by Guy Matz on 4/6/21.
//

#include "Rectangle.hpp"
#include <cmath>
#include <iostream>

/**
 * Parameterized constructor; takes in width and height,
   iterates through the 2D array to populate it with
   the necessary characters given the parameter dimensions
 * @param side
 */
Rectangle::Rectangle(const int& width, const int& height) : Shape(width, height) {
    setEdges(4);

    // Populate 2D array with empty chars
    char **arr = new char *[getHeight()];
    for (int row = 0; row < getHeight(); row++)
    {
        arr[row] = new char[getWidth()];
        for (int col = 0; col < getWidth(); col++)
        {
            arr[row][col] = ' ';
        }
    }

    // Populate the proper positions with *'s
    char ascii_counter = 48;

    for (int row = 0; row < getHeight(); row++)
    {
        for (int col = 0; col < getWidth(); col++)
        {
            // we populate first column, last row & hypostenuse
            if (col == 0 or col == (getWidth()-1) or row == 0 or row==(getHeight()-1))
            {
                arr[row][col] = ascii_counter;

                // fix ascii_counter to wrap around after
                ascii_counter++;
                if (ascii_counter > 126)
                {
                    ascii_counter = 48;
                }
            }
        }
    }
    setDisplayChars(arr);
}

double Rectangle::getSurfaceArea() {
    return getWidth() * getHeight();
}

double Rectangle::get3DVolume(const double &depth) {
    return getSurfaceArea() * depth;
}

// Mutators
//rotate by 90 degrees
/**
     @post shifts matrix 90 degrees clockwise
 */
void Rectangle::rotateRight() {

    // Populate sideways 2D array with empty chars
    char **arr = new char *[getWidth()];
    for (int row = 0; row < getWidth(); row++)
    {
        arr[row] = new char[getHeight()];
        for (int col = 0; col < getHeight(); col++)
        {
            arr[row][col] = ' ';
        }
    }

    // shift rows to columns in new array, arr
    int new_row, new_col;
    for (int row=0; row < getHeight(); row++) {
        for (int col=0; col < getWidth(); col++) {
            new_row = col;
            new_col = getHeight() - row - 1;
            arr[new_row][new_col] = matrix_[row][col];
        }
    }

    // set dimensions to new array
    int w = getWidth();
    int h = getHeight();
    setHeight(w);
    setWidth(h);

    matrix_ = arr;

}

//rotate by 90 degrees
/**
     @post shifts matrix 90 degrees counterclockwise
 */
void Rectangle::rotateLeft() {

    // Populate sideways 2D array with empty chars
    char **arr = new char *[getWidth()];
    for (int row = 0; row < getWidth(); row++)
    {
        arr[row] = new char[getHeight()];
        for (int col = 0; col < getHeight(); col++)
        {
            arr[row][col] = ' ';
        }
    }

    // shift rows to columns in new array, arr
    int new_row, new_col;
    for (int row=0; row < getHeight(); row++) {
        for (int col=0; col < getWidth(); col++) {
            new_row = getWidth() - col - 1;
            new_col = row;
            arr[new_row][new_col] = matrix_[row][col];
        }
    }

    // set dimensions to new array
    int w = getWidth();
    int h = getHeight();
    setHeight(w);
    setWidth(h);

    matrix_ = arr;

}

//reflect over x or y axis
/**
     @param axis: axis to shift matrix about
     @post shifts matrix across the supplied axis
 */
void Rectangle::reflect(char axis) {
    //std::cout << "In t reflect " << axis << std::endl;
    //std::cout << "In reflect" << std::endl;
    // Populate 2D array with empty chars
    char **arr = new char *[getHeight()];
    for (int row = 0; row < getHeight(); row++)
    {
        arr[row] = new char[getWidth()];
        for (int col = 0; col < getWidth(); col++)
        {
            arr[row][col] = ' ';
        }
    }

    if (axis == 'x') {
        //std::cout << "Going height from 0 to " << getHeight() << std::endl;
        //std::cout << "Going width from 0 to " << getWidth() << std::endl;
        for (int col = 0; col < getWidth(); col++) {
            for (int row = 0; row < getHeight(); row++) {
                //std::cout << "[" << row << "," << getHeight() - col - 1 << "] = [" << row << "," << col << "], " ;
                arr[row][col] = matrix_[getHeight() - row - 1][col];
            }
            //std::cout << std::endl;
        }
        //std::cout << std::endl << "HERE" << std::endl;
    }
    else {
        for (int row = 0; row < getHeight() ; row++) {
            for (int col = 0; col < getWidth() ; col++) {
                arr[row][getWidth() - col - 1] = matrix_[row][col];
            }
        }

    }
    // shift rows to columns in new array, arr

    matrix_ = arr;
}
