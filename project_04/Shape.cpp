//
// Created by Guy Matz on 4/5/21.
//

#include <iostream>
#include "Shape.hpp"
// Parameterized Constructor; there is no default constructor
/**
     Parameterized Constructor
     @param width   : width of the shape
     @param height  : height of the shape
 */
Shape::Shape(const int &width, const int &height) : width_(width), height_(height) {
    char** matrix_ = new char*[height_];

    for (int i = 0; i < height_; i++) {
        matrix_[i] = new char[width_];
    }
}

// Getters
/**
     @return num_edges_  : The number of edges for the shape
 */
int Shape::getEdges() {
    return num_edges_;
}

/**
     @return width_  : The width of the shape
 */
int Shape::getWidth() {
    return width_;
}

/**
     @return height_  : The height of the shape
 */
int Shape::getHeight() {
    return height_;
}

/**
     @return matrix_  : The matrix representing the shape
 */
char** Shape::getDisplayChars() {
    return matrix_;
}

// Setters
/**
     @param edges  : Number of edges for shape
 */
void Shape::setEdges(const int& edges) {
    num_edges_ = edges;
}

/**
     @param new_width  : width for shape
 */
void Shape::setWidth(const int& new_width) {
    width_ = new_width;
}

/**
     @param new_height  : height for shape
 */
void Shape::setHeight(const int &new_height) {
    height_ = new_height;
}

/**
     @param display  : matrix of shape to be displayed
 */
void Shape::setDisplayChars(char **display) {
    //std::cout << "In setDisplayChars" << std::endl;
    matrix_ = display;
}

// Mutators
//rotate by 90 degrees
/**
     @post shifts matix 90 degrees clockwise
 */
void Shape::rotateRight() {

    // Populate 2D array with empty chars
    char **arr = new char *[getHeight()];
    for (int row = 0; row < getHeight(); row++)
    {
        arr[row] = new char[getWidth()];
        for (int col = 0; col < getWidth(); col++)
        {
            arr[row][col] = ' ';
        }
    }
    std::cout << "Height = " << getHeight() << ", Width = " << getWidth() << std::endl;

    // shift rows to columns in new array, arr
    for (int row=getHeight()-1; row >= 0; row--) {
        //std::cout << row << " ";
        for (int col=0; col <= getWidth()-1; col++) {
            arr[col][getHeight() - row - 1] = matrix_[row][col];
        }
    }

    matrix_ = arr;

}

//rotate by 90 degrees
/**
     @post shifts matix 90 degrees counterclockwise
 */
void Shape::rotateLeft() {
    // Populate 2D array with empty chars
    char **arr = new char *[getHeight()];
    for (int row = 0; row < getHeight(); row++)
    {
        arr[row] = new char[getWidth()];
        for (int col = 0; col < getWidth(); col++)
        {
            arr[row][col] = ' ';
        }
    }

    // shift rows to columns in new array, arr
    for (int row=getHeight()-1; row >= 0; row--) {
        for (int col=0; col <= getWidth()-1; col++) {
            arr[getHeight() - col - 1][row] = matrix_[row][col];
        }
    }

    matrix_ = arr;
}

//reflect over x or y axis
/**
     @param axis: axis to shift matrix about
     @post shifts matix across the supplied axis
 */
void Shape::reflect(char axis) {
    //std::cout << "In reflect" << std::endl;
    // Populate 2D array with empty chars
    char **arr = new char *[getHeight()];
    for (int row = 0; row < getHeight(); row++)
    {
        arr[row] = new char[getWidth()];
        for (int col = 0; col < getWidth(); col++)
        {
            arr[row][col] = ' ';
        }
    }

    if (axis == 'x') {
        for (int row = 0; row < getHeight() ; row++) {
            for (int col = 0; col < getWidth() ; col++) {
                arr[getHeight() - row - 1][col] = matrix_[row][col];
            }
        }
    }
    else {
        for (int col = 0; col <= getWidth() - 1; col++) {
            for (int row = 0; row <= getHeight() - 1; row++) {
                arr[row][getHeight() - col - 1] = matrix_[row][col];
            }
        }

    }
    // shift rows to columns in new array, arr

    matrix_ = arr;
}

// Display - //iterate through 2D array and print chars
/**
     print matrix
 */
void Shape::display() {
    //std::cout << "In display" << std::endl;
    for (int i = 0; i < height_; i++) {
        for (int j = 0; j < width_; j++) {

            // Print the values of
            // memory blocks created
            std::cout << matrix_[i][j];
            if (j < width_ - 1) {
                std::cout << " ";
            }
        }
        std::cout << std::endl;
    }
}