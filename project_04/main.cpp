#include <iostream>
#include "Circle.hpp"
#include "Triangle.hpp"
#include "Rectangle.hpp"

int main() {
    /*
    std::cout << "Hello, World!" << std::endl;

    Circle c = Circle(8);
    std::cout << "Circle c: " << c.getSurfaceArea() << " " << c.get3DVolume(2) << std::endl;
    c.display();
    c.rotateRight();
    c.display();
    c.rotateLeft();
    c.display();
    c.rotateLeft();
    c.display();
    c.rotateRight();
    c.reflect('x');
    c.display();
    c.reflect('y');
    c.display();

    Triangle t = Triangle(7);
    std::cout << "Triangle t: " << t.getSurfaceArea() << " " << t.get3DVolume(2) << std::endl;
    t.display();
    t.rotateRight();
    t.display();
    t.rotateLeft();
    t.display();
    t.rotateLeft();
    t.display();
    t.rotateRight();
    t.display();
    t.reflect('x');
    t.display();
    t.reflect('y');
    t.display();
    */

    Rectangle r1 = Rectangle(15, 7);
    std::cout << "Rectangle r: " << r1.getSurfaceArea() << " " << r1.get3DVolume(2) << std::endl;
    r1.display();
    r1.reflect('x');
    r1.display();
    r1.reflect('y');
    r1.display();

    Rectangle r2 = Rectangle(12, 6);
    std::cout << "Rectangle r: " << r2.getSurfaceArea() << " " << r2.get3DVolume(2) << std::endl;
    r2.display();
    r2.reflect('y');
    r2.display();
    r2.reflect('x');
    r2.display();
    std::cout << "rotating right" << std::endl;
    r2.rotateRight();
    std::cout << "displaying" << std::endl;
    r2.display();
    std::cout << "rotating left" << std::endl;
    r2.rotateLeft();
    std::cout << "displaying" << std::endl;
    r2.display();
    r2.rotateLeft();
    r2.display();
    r2.rotateRight();
    r2.display();

    return 0;
}
