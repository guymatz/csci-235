//
// Created by Guy Matz on 4/6/21.
//

#ifndef PROJECT_04_RECTANGLE_HPP
#define PROJECT_04_RECTANGLE_HPP

#include "Shape.hpp"

class Rectangle : public Shape {
public:
    /**
     * Parameterized constructor; takes in width and height,
     * iterates through the 2D array to populate it with
     * the necessary characters given the parameter dimensions
    */
    Rectangle(const int& width, const int& height);
    void rotateRight();
    void rotateLeft();
    void reflect(char axis);

    double getSurfaceArea();
    double get3DVolume(const double &depth);
};


#endif //PROJECT_04_RECTANGLE_HPP
