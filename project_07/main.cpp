#include <iostream>
#include "BinaryNode.hpp"
#include "PriorityQueueBST.hpp"

int main() {
    std::cout << "Creating / Testing Nodes . . . " << std::endl;
    BinaryNode<int> bn1;
    BinaryNode<int> bn2 = BinaryNode(3);
    bn1.setItem(5);
    bn1.setLeftChildPtr(&bn2);
    bn2.setLeftChildPtr(&bn1);
    bn1.setRightChildPtr(&bn2);
    bn2.setRightChildPtr(&bn1);

    std::cout << "Creating BST Prioritized Queue . . . " << std::endl;
    PriorityQueueBST<int> pqbst;
    pqbst.add(5);
    std::cout << "Height: " << pqbst.getHeight() << ", Number: " << pqbst.getNumberOfNodes() << std::endl;
    pqbst.add(9);
    std::cout << "Height: " << pqbst.getHeight() << ", Number: " << pqbst.getNumberOfNodes() << std::endl;
    pqbst.add(3);
    std::cout << "Height: " << pqbst.getHeight() << ", Number: " << pqbst.getNumberOfNodes() << std::endl;
    pqbst.add(4);
    std::cout << "Height: " << pqbst.getHeight() << ", Number: " << pqbst.getNumberOfNodes() << std::endl;
    pqbst.add(2);
    std::cout << "Height: " << pqbst.getHeight() << ", Number: " << pqbst.getNumberOfNodes() << std::endl;
    pqbst.add(20);
    std::cout << "Height: " << pqbst.getHeight() << ", Number: " << pqbst.getNumberOfNodes() << std::endl;
    std::cout << "Clearing . . . " << std::endl;
    //pqbst.clear();
    return 0;
}
