/****************************************************************************************************************************
Title        : BinaryNode.cpp
Auhor        : Guy Matz
Description  : implementation file of a Node class for a Queue
Dependencies : BinaryNode.hpp
****************************************************************************************************************************/
#include "BinaryNode.hpp"

/* Default Constructor */
template <class ItemType>
BinaryNode<ItemType>::BinaryNode() : left_{nullptr},right_{nullptr},item_{}
{
}

/* Parameterized Constructor accepting just the item*/
template <class ItemType>
BinaryNode<ItemType>::BinaryNode(const ItemType &an_item) : item_{an_item},left_{nullptr},right_{nullptr}
{
}

/* Setter: item_ */
template <class ItemType>
void BinaryNode<ItemType>::setItem(const ItemType &anItem)
{
    item_ = anItem;
}

/* Setter: left */
template <class ItemType>
void BinaryNode<ItemType>::setLeftChildPtr(BinaryNode<ItemType> *left_node_ptr)
{
    left_ = left_node_ptr;
}

/* Setter: right */
template <class ItemType>
void BinaryNode<ItemType>::setRightChildPtr(BinaryNode<ItemType> *right_node_ptr)
{
    right_ = right_node_ptr;
}

/* Accessor: item_ */
template <class ItemType>
ItemType BinaryNode<ItemType>::getItem() const
{
    return item_;
}

/* Accessor: left_ */
template <class ItemType>
BinaryNode<ItemType> *BinaryNode<ItemType>::getLeftChildPtr() const
{
    return left_;
}

/* Accessor: right_ */
template <class ItemType>
BinaryNode<ItemType> *BinaryNode<ItemType>::getRightChildPtr() const
{
    return right_;
}

/**
 * checks is node has descendants
 * @tparam ItemType
 * @return bool
 */
template <class ItemType>
bool BinaryNode<ItemType>::isLeaf() const {
    return ((left_ == nullptr) and (right_ == nullptr));
}