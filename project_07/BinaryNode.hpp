#ifndef BINARY_NODE_H_
#define BINARY_NODE_H_
template<typename ItemType> class BinaryNode
{
public:
  BinaryNode();
  BinaryNode(const ItemType& an_item);
  void setItem(const ItemType& an_item);
  ItemType getItem() const;

  bool isLeaf() const;

  BinaryNode<ItemType>* getLeftChildPtr() const;
  BinaryNode<ItemType>* getRightChildPtr() const;

  void setLeftChildPtr(BinaryNode<ItemType>* left_ptr);
  void setRightChildPtr(BinaryNode<ItemType>* right_ptr);
private:
  ItemType item_; // A data item
  BinaryNode<ItemType>* left_; // Pointer to next node on left
  BinaryNode<ItemType>* right_; // Pointer to next node on right
}; // end BinaryNode
#include "BinaryNode.cpp"
#endif // BINARY_NODE_H_