//
// Created by Guy Matz on 5/17/21.
//

#include "BinaryNode.hpp"
#include "PriorityQueueBST.hpp"
#include <algorithm>
#include <iostream>

/**
 * Constructor points top node of tree to nullptr
 * @tparam ItemType
 */
template<typename ItemType>
PriorityQueueBST<ItemType>::PriorityQueueBST() :top_(nullptr) {}

/**
 * Destructor which calls destroyTree
 * @tparam ItemType
 */
template<typename ItemType>
PriorityQueueBST<ItemType>::~PriorityQueueBST() {
    destroyTree(top_);
}

/**
 * Checks for empty tree
 * @tparam ItemType
 * @return  boolean of whether or not tree is empty
 */
template<typename ItemType>
bool PriorityQueueBST<ItemType>::isEmpty() const {
    if (top_->getLeftChildPtr() == nullptr and top_->getRightChildPtr() == nullptr)
        return true;
    else
        return false;
}

/**
 * Calls getHeightHelper
 * @tparam ItemType
 * @return height of longest branch in tree
 */
template <typename ItemType>
size_t PriorityQueueBST<ItemType>::getHeight() const {
    return getHeightHelper(top_);
}

/**
 * recursive function to determine height of longest branch in tree
 * @tparam ItemType
 * @param sub_tree_ptr
 * @return height of longest branch in tree
 */
template <typename ItemType>
size_t PriorityQueueBST<ItemType>::getHeightHelper(const BinaryNode<ItemType>* sub_tree_ptr) const {
    if (sub_tree_ptr == nullptr)
        return 0;
    else
        return 1 + std::max(getHeightHelper(sub_tree_ptr->getLeftChildPtr()),
                                            getHeightHelper(sub_tree_ptr->getRightChildPtr())
                           );
}

/**
 * Calls getNumberOfNodesHelper
 * @tparam ItemType
 * @return number of nodes on BST
 */
template <typename ItemType>
size_t PriorityQueueBST<ItemType>::getNumberOfNodes() const {
    return getNumberOfNodesHelper(top_);
}

/**
 * Recursive function that determines number of nodes in tree
 * @tparam ItemType
 * @param sub_tree_ptr
 * @return  number of nodes in BST
 */
template <typename ItemType>
size_t PriorityQueueBST<ItemType>::getNumberOfNodesHelper(const BinaryNode<ItemType>* sub_tree_ptr) const {
    if (sub_tree_ptr == nullptr)
        return 0;
    else {
        return 1 + getNumberOfNodesHelper(sub_tree_ptr->getLeftChildPtr()) +
                   getNumberOfNodesHelper(sub_tree_ptr->getRightChildPtr());
    }
}

/**
 * recurve function that copies nodes from passed-in tree to this tree
 * @tparam ItemType
 * @param old_tree_root_ptr
 * @return
 */
template <typename ItemType>
BinaryNode<ItemType>* PriorityQueueBST<ItemType>::copyTree(const BinaryNode<ItemType>* old_tree_root_ptr) const {
    BinaryNode new_tree_ptr = nullptr;
    if (old_tree_root_ptr != nullptr) {
        new_tree_ptr = BinaryNode<ItemType>(old_tree_root_ptr->getItem());
        new_tree_ptr.setLeftChildPtr(copyTree(old_tree_root_ptr->getLeftChildPtr()));
        new_tree_ptr.setRightChildPtr(copyTree(old_tree_root_ptr->getRightChildPtr()));
    }
    return new_tree_ptr;
}

/**
 * Recursive function that deletes leafs (leaves)
 * @tparam ItemType
 * @param sub_tree_ptr
 */
template <typename ItemType>
void PriorityQueueBST<ItemType>::destroyTree(BinaryNode<ItemType>* sub_tree_ptr) const {
    if (sub_tree_ptr != nullptr) {
        //std::cout << "I see " << sub_tree_ptr << std::endl;
        destroyTree(sub_tree_ptr->getLeftChildPtr());
        destroyTree(sub_tree_ptr->getRightChildPtr());
        std::cout << "Deleting " << sub_tree_ptr << std::endl;
        delete sub_tree_ptr;
        std::cout << "Nulling " << sub_tree_ptr << std::endl;
        sub_tree_ptr = nullptr;
    }
}

/**
 * Calls destroyTree, passing in the top node of the BST
 * @tparam ItemType
 */
template <typename ItemType>
void PriorityQueueBST<ItemType>::clear() const {
    destroyTree(top_);
}

/**
 * Calls placeNode
 * @tparam ItemType
 * @param item
 */
template <typename ItemType>
void PriorityQueueBST<ItemType>::add(const ItemType& item) {
    BinaryNode<ItemType>* new_node_ptr = new BinaryNode<ItemType>(item);
    //std::cout << "Adding " << new_node_ptr->getItem() << std::endl;
    if (top_ != nullptr) {
        //std::cout << "Top = " << top_->getItem() << std::endl;
    }
    top_ = placeNode(top_, new_node_ptr);
}

/**
 * Recursive function that places new BinaryNode in proper spot in BST
 * @tparam ItemType
 * @param subtree_ptr
 * @param new_node_ptr
 * @return
 */
template <typename ItemType>
BinaryNode<ItemType>* PriorityQueueBST<ItemType>::placeNode(BinaryNode<ItemType>* subtree_ptr, BinaryNode<ItemType>* new_node_ptr) {
    //std::cout << "Here " << " " << new_node_ptr->getItem() << std::endl;
    if (subtree_ptr == nullptr){
        //std::cout << "Here null " << new_node_ptr->getItem() << std::endl;
        return new_node_ptr;
    }
    else {
        //std::cout << "I see " << subtree_ptr->getItem() << std::endl;
        if (subtree_ptr->getItem() > new_node_ptr->getItem()) {
            BinaryNode<ItemType>* tmp_ptr = placeNode(subtree_ptr->getLeftChildPtr(), new_node_ptr);
            //std::cout << "Placing " << new_node_ptr->getItem() <<  " to the left of " << subtree_ptr->getItem() << std::endl;
            subtree_ptr->setLeftChildPtr(tmp_ptr);
        }
        else {
            BinaryNode<ItemType>* tmp_ptr = placeNode(subtree_ptr->getRightChildPtr(), new_node_ptr);
            //std::cout << "Placing " << tmp_ptr->getItem() <<  " to the right of " << subtree_ptr->getItem() << std::endl;
            subtree_ptr->setRightChildPtr(tmp_ptr);
        }
        return subtree_ptr;
    }
}

/**
 * Calls removeValue with node to remove
 * @tparam ItemType
 * @param target
 * @return is_successful - boolean of whether the remove worked
 */
template <typename ItemType>
bool PriorityQueueBST<ItemType>::remove(const ItemType& target) {
    bool is_successful = false;
    top_ = removeValue(top_, target, is_successful);
    return is_successful;
}

/**
 * Recusrsively searches tree for value to remove
 * @tparam ItemType
 * @param subtree_ptr
 * @param target
 * @param success
 * @return pointer to node to remove
 */
template <typename ItemType>
BinaryNode<ItemType>* PriorityQueueBST<ItemType>::removeValue(BinaryNode<ItemType>* subtree_ptr, const ItemType target, bool& success) {
    if (subtree_ptr == nullptr){
        success = false;
        return subtree_ptr;
    }
    if (subtree_ptr->getItem() == target) {
        subtree_ptr = removeNode(subtree_ptr);
        success = true;
        return subtree_ptr;
    }
    else {
        if (subtree_ptr->getItem() > target) {
            subtree_ptr->setLeftChildPtr(removeValue(subtree_ptr->getLeftChildPtr(), target, success));
        }
        else {
            subtree_ptr->setRightChildPtr(removeValue(subtree_ptr->getRightChildPtr(), target, success));
        }
        return subtree_ptr;
    }
}

/**
 * Recursive function the looks for Node on left and removes if found
 * @tparam ItemType
 * @param node_ptr
 * @param inorderSuccessor
 * @return  node_ptr, node to remove
 */
template <typename ItemType>
BinaryNode<ItemType>* PriorityQueueBST<ItemType>::removeNode(BinaryNode<ItemType>* node_ptr) {
    if (node_ptr->isLeaf()){
        delete node_ptr;
        node_ptr = nullptr;
    }
    else if (node_ptr->getLeftChildPtr() == nullptr) {
        return node_ptr->getRightChildPtr();
    }
    else if (node_ptr->getRightChildPtr() == nullptr) {
        return node_ptr->getLeftChildPtr();
    }
    else {
        ItemType new_node_value;
        node_ptr->setRightChildPtr(removeLeftmostNode(node_ptr->getRightChildPtr(), new_node_value));
        node_ptr->setItem(new_node_value);
        return node_ptr;
    }
}

/**
 * Recursive function the looks for Node on left and removes if found
 * @tparam ItemType
 * @param node_ptr
 * @param inorderSuccessor
 * @return  node_ptr, node to remove
 */
template <typename ItemType>
BinaryNode<ItemType>* PriorityQueueBST<ItemType>::removeLeftmostNode(BinaryNode<ItemType>* node_ptr, ItemType& inorderSuccessor) {
    if (node_ptr->getLeftChildPtr() == nullptr){
        inorderSuccessor = node_ptr->getItem();
        return removeNode(node_ptr);
    }
    else {
        node_ptr->setLeftChildPtr(removeLeftmostNode(node_ptr->getLeftChildPtr(), inorderSuccessor));
        return node_ptr;
    }
}

/**
 * calls preorder with top of BST
 * @tparam ItemType
 */
template<typename ItemType>
void PriorityQueueBST<ItemType>::preorderTraverse() const {
    preorder(top_);
}

/**
 * calls inorder with top of BST
 * @tparam ItemType
 */
template<typename ItemType>
void PriorityQueueBST<ItemType>::inorderTraverse() const {
    inorder(top_);
}

/**
 * calls postorder with top of BST
 * @tparam ItemType
 */
template<typename ItemType>
void PriorityQueueBST<ItemType>::postorderTraverse() const {
    postorder(top_);
}

/**
 * Outputs items in tree in preorder
 * @tparam ItemType
 * @param tree_ptr
 */
template<typename ItemType>
void PriorityQueueBST<ItemType>::preorder(BinaryNode<ItemType>* tree_ptr) const {
    if (tree_ptr != nullptr) {
        preorder(tree_ptr->getLeftChildPtr());
        std::cout << tree_ptr->getItem() << std::endl;
        preorder(tree_ptr->getRightChildPtr());
    }
}

/**
 * Outputs items in tree in order
 * @tparam ItemType
 * @param tree_ptr
 */
template<typename ItemType>
void PriorityQueueBST<ItemType>::inorder(BinaryNode<ItemType>* tree_ptr) const {
    if (tree_ptr != nullptr) {
        std::cout << tree_ptr->getItem() << std::endl;
        inorder(tree_ptr->getLeftChildPtr());
        inorder(tree_ptr->getRightChildPtr());
    }
}

/**
 * Outputs items in tree in postorder
 * @tparam ItemType
 * @param tree_ptr
 */
template<typename ItemType>
void PriorityQueueBST<ItemType>::postorder(BinaryNode<ItemType>* tree_ptr) const {
    if (tree_ptr != nullptr) {
        postorder(tree_ptr->getLeftChildPtr());
        postorder(tree_ptr->getRightChildPtr());
        std::cout << tree_ptr->getItem() << std::endl;
    }
}