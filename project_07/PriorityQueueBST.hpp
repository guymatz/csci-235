//
// Created by Guy Matz on 5/17/21.
//

#ifndef PROJECT_07_PRIORITYQUEUEBST_HPP
#define PROJECT_07_PRIORITYQUEUEBST_HPP

#include <cstdlib>
#include "BinaryNode.hpp"

template<typename ItemType>
class PriorityQueueBST
{
public:
    PriorityQueueBST();
    //PriorityQueueBST(const PriorityQueueBST<ItemType>& tree);
    ~PriorityQueueBST();
    bool isEmpty() const; // returns true if no elements in the tree
    std::size_t getHeight() const; // returns the length of longest branch in tree
    std::size_t getNumberOfNodes() const; // returns the number of elements in the queue
    void add(const ItemType& item); //adds by priority
    bool remove(const ItemType& target); // removes
    //ItemType find(const ItemType& item) const; // returns a copy of the front element
    void clear() const; // calls destroyTree
    void preorderTraverse() const; //  calls preorder
    void inorderTraverse() const; // calls inorder
    void postorderTraverse() const; // calls postorder
    void preorder(BinaryNode<ItemType>* tree_ptr) const; // outputs subchildren in preorder
    void inorder(BinaryNode<ItemType>* tree_ptr) const; //  outputs subchildren in eorder
    void postorder(BinaryNode<ItemType>* tree_ptr) const; //  outputs subchildren in postorder
    //PriorityQueueBST& operator=(const PriorityQueueBST<ItemType>& rhs);

private:
    BinaryNode<ItemType>* copyTree(const BinaryNode<ItemType>* old_tree_root_ptr) const; // copies input tree to self
    size_t getHeightHelper(const BinaryNode<ItemType>* sub_tree_ptr) const; // returns length of longest branch
    size_t getNumberOfNodesHelper(const BinaryNode<ItemType>* sub_tree_ptr) const; // returns number of nodes in tree
    void destroyTree(BinaryNode<ItemType>* top) const; // removes all leafs (leaves) of tree
    BinaryNode<ItemType>* placeNode(BinaryNode<ItemType>* sub_tree_ptr, BinaryNode<ItemType>* new_node); // inserts node
    BinaryNode<ItemType>* removeValue(BinaryNode<ItemType>* sub_tree_ptr, const ItemType target, bool& sucess); // removes node
    BinaryNode<ItemType>* top_; // Pointer to top of the tree // ptr to top of tree
    BinaryNode<ItemType>* removeNode(BinaryNode<ItemType>* node_ptr); // removes node in tree
    BinaryNode<ItemType>* removeLeftmostNode(BinaryNode<ItemType>* node_ptr, ItemType& inorderSuccessor); //removes leftmost node in tree
}; //end PriorityQueueBST

#include "PriorityQueueBST.cpp"
#endif // PROJECT_07_PRIORITYQUEUEBST_HPP
